const { role } = require("../constants/role.constant");

const isAdmin = async (req, res, next) => {
    if(req.userRole == role.ADMIN) {
        next();
    }
}

module.exports = { isAdmin }