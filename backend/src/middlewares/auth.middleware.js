const jwt = require('jsonwebtoken');
const apiStatus = require('../constants/apiStatus.constant');
const httpStatus = require('../constants/httpStatus.constant');

const { verify, TokenExpiredError } = jwt;

const CatchExpiredTokenError = (err, res) => {
    if (err instanceof TokenExpiredError) {
        return res.status(httpStatus.UNAUTHORIZED).send({
            status: apiStatus.AUTH_ERROR,
            message: 'Unauthorized! Token was expired',
        });
    }
    return res.status(httpStatus.UNAUTHORIZED).send({
        status: apiStatus.AUTH_ERROR,
        message: 'Unauthorized! Invalid token',
    });
};

const verifyToken = async (req, res, next) => {
    let bearerToken = req.headers.authorization;
    if (bearerToken !== undefined && bearerToken.startsWith('Bearer ')) {
        let token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(httpStatus.FORBIDDEN).send({
                status: apiStatus.AUTH_ERROR,
                message: 'No token provide!',
            });
        }

        verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return CatchExpiredTokenError(err, res);
            }
            req.userId = decoded.id;
            req.userRole = decoded.role;
            next();
        });
    } else {
        return res.status(httpStatus.FORBIDDEN).send({
            status: apiStatus.AUTH_ERROR,
            message: 'Invalid token provide!',
        });
    }
};

module.exports = { verifyToken };
