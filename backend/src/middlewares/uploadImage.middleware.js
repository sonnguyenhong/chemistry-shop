const multer = require('multer');
const path = require('path');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const uploadsPath = 'uploads';
        fs.mkdirSync(path.join(__dirname, '../..', uploadsPath), { recursive: true });
        cb(null, path.join(__dirname, '../..', uploadsPath));
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
        cb(null, file.fieldname + '-' + uniqueSuffix + '.jpg');
    },
});

const uploadImage = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype.substring(0, 6) !== 'image/') {
            return cb(new Error('Wrong file type'));
        }
        cb(null, true);
    },
});

module.exports = { uploadImage };
