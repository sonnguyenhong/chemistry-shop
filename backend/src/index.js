const express = require('express');
const path = require('path');
require('dotenv').config();
const Admin = require('./models').Admin;
const route = require('./routes/index');
const { hashSync } = require('bcryptjs');

const app = express();
const PORT = process.env.PORT;

const db = require('./models');

app.use(express.json());
app.use(express.static(path.join(__dirname, '..', 'uploads')));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

route(app);

db.sequelize
    .sync()
    .then(() => {
        console.log('Sync database successfully!');
        app.listen(PORT, () => {
            console.log(`Server is running at http://localhost:${PORT}`);
        });
    })
    .catch((error) => {
        console.log('Fail to sync database: ' + error.message);
    });
