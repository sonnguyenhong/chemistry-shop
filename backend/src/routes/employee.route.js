const express = require('express');

const { verifyToken } = require('../middlewares/auth.middleware');
const { isAdmin } = require('../middlewares/permission.middleware');
const { create, getAll, getById } = require('../controllers/employee.controller');

const router = express.Router();

router.get('/:id', verifyToken, isAdmin, getById);
router.get('/', verifyToken, isAdmin, getAll);
router.post('/', verifyToken, isAdmin, create);

module.exports = router;
