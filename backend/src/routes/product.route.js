const express = require('express');

const { getAll, getById, create, update, remove } = require('../controllers/product.controller');
const { verifyToken } = require('../middlewares/auth.middleware');
const { uploadImage } = require('../middlewares/uploadImage.middleware');

const router = express.Router();

router.get('/', getAll);
router.get('/:id', verifyToken, getById);
router.post('/', verifyToken, uploadImage.single('image'), create);
router.put('/:id', verifyToken, uploadImage.single('image'), update);
router.delete('/:id', verifyToken, remove);

module.exports = router;
