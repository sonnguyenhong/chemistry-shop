const authRoutes = require('./auth.route');
const productRoutes = require('./product.route');
const orderRoutes = require('./order.route');
const customerRoutes = require('./customer.route');
const employeeRoutes = require('./employee.route');

const route = (app) => {
    app.use('/api/v1/auth', authRoutes);
    app.use('/api/v1/products', productRoutes);
    app.use('/api/v1/orders', orderRoutes);
    app.use('/api/v1/customers', customerRoutes);
    app.use('/api/v1/employees', employeeRoutes);
};

module.exports = route;
