const express = require('express');

const { verifyToken } = require('../middlewares/auth.middleware');
const { isAdmin } = require('../middlewares/permission.middleware');
const { create, getAll, getById, update, remove } = require('../controllers/customer.controller');

const router = express.Router();

router.get('/:id', verifyToken, isAdmin, getById);
router.get('/', verifyToken, isAdmin, getAll);
router.post('/', verifyToken, isAdmin, create);
router.put('/:id', verifyToken, isAdmin, update);
router.delete('/:id', verifyToken, isAdmin, remove);

module.exports = router;
