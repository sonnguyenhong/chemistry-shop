const express = require('express');

const { verifyToken } = require('../middlewares/auth.middleware');
const { create, getAll, getById, update } = require('../controllers/order.controller');

const router = express.Router();

router.get('/:id', verifyToken, getById);
router.get('/', verifyToken, getAll);
router.post('/', verifyToken, create);
router.put('/:id', verifyToken, update);

module.exports = router;
