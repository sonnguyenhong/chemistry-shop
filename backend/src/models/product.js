'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Product extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsToMany(models.Test, {
                through: 'TestProduct',
                foreignKey: 'productId',
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
            });
            this.belongsTo(models.Category, {
                foreignKey: 'categoryId',
            });
            // this.belongsToMany(models.Order, {
            //     through: 'OrderDetail',
            //     foreignKey: 'productId',
            //     onDelete: 'CASCADE',
            //     onUpdate: 'CASCADE',
            // });
            this.hasMany(models.OrderDetail, {
                foreignKey: 'productId',
            });
        }
    }
    Product.init(
        {
            productId: {
                type: DataTypes.STRING,
                unique: true,
                require: true,
            },
            productCode: {
                type: DataTypes.STRING,
                unique: true,
                require: true,
            },
            englishName: {
                type: DataTypes.STRING,
            },
            vietnameseName: {
                type: DataTypes.STRING,
            },
            characteristics: {
                type: DataTypes.STRING,
            },
            brand: {
                type: DataTypes.STRING,
            },
            origin: {
                type: DataTypes.STRING,
            },
            serial: {
                type: DataTypes.STRING,
            },
            quantity: {
                type: DataTypes.INTEGER,
            },
            expireDate: {
                type: DataTypes.DATE,
            },
            importDate: {
                type: DataTypes.DATE,
            },
            warehouse: {
                type: DataTypes.STRING,
            },
            storageCondition: {
                type: DataTypes.STRING,
            },
            price: {
                type: DataTypes.STRING,
            },
            imageUrl: {
                type: DataTypes.STRING,
            },
        },
        {
            sequelize,
            modelName: 'Product',
        },
    );
    return Product;
};
