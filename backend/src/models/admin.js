'use strict';
const { Model } = require('sequelize');
const { genSaltSync, hashSync } = require('bcryptjs');
module.exports = (sequelize, DataTypes) => {
    class Admin extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Admin.init(
        {
            username: {
                type: DataTypes.STRING,
                unique: true,
                require: true,
            },
            password: {
                type: DataTypes.STRING,
                require: true,
            },
            name: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: 'Admin',
            hooks: {
                beforeCreate: (user, options) => {
                    const salt = genSaltSync(10);
                    user.password = hashSync(user.password, salt);
                },
                beforeBulkCreate: (users, options) => {
                    const salt = genSaltSync(10);
                    for (const user of users) {
                        user.password = hashSync(`${user.password}`, salt);
                    }
                },
            },
        },
    );
    return Admin;
};
