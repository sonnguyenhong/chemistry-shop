'use strict';
const { Model } = require('sequelize');
const { genSaltSync, hashSync } = require('bcryptjs');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.hasMany(models.Order, {
                foreignKey: 'userId',
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
            });
        }
    }
    User.init(
        {
            username: {
                type: DataTypes.STRING,
                unique: true,
                require: true,
            },
            email: {
                type: DataTypes.STRING,
                unique: true,
                require: true,
            },
            password: {
                type: DataTypes.STRING,
                require: true,
            },
            phone: {
                type: DataTypes.STRING,
            },
            name: DataTypes.STRING,
            code: DataTypes.STRING,
            property: DataTypes.STRING,
            level: DataTypes.ENUM('Tuyến tỉnh', 'PK tư nhân', 'Tuyến huyện', 'Tuyến Trung ương'),
            address: DataTypes.STRING,
            status: DataTypes.ENUM('PENDING', 'ACTIVE', 'INACTIVE'),
        },
        {
            sequelize,
            modelName: 'User',
            hooks: {
                beforeCreate: (user, options) => {
                    const salt = genSaltSync(10);
                    user.password = hashSync(user.password, salt);
                },
                beforeBulkCreate: (users, options) => {
                    const salt = genSaltSync(10);
                    for (const user of users) {
                        user.password = hashSync(`${user.password}`, salt);
                    }
                },
            },
        },
    );
    return User;
};
