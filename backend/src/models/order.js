'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Order extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            // this.belongsToMany(models.Product, {
            //     through: 'OrderDetail',
            //     foreignKey: 'orderId',
            //     onDelete: 'CASCADE',
            //     onUpdate: 'CASCADE',
            // });
            this.hasMany(models.OrderDetail, {
                foreignKey: 'orderId',
            });
            this.belongsTo(models.User, {
                foreignKey: 'userId',
            });
        }
    }
    Order.init(
        {
            code: {
                type: DataTypes.STRING,
                require: true,
            },
            date: {
                type: DataTypes.STRING,
                require: true,
            },
            totalPrice: {
                type: DataTypes.STRING,
                require: true,
            },
            status: {
                type: DataTypes.ENUM('PENDING', 'ACCEPT', 'PREPARING', 'DELIVERING', 'DELIVERED', 'CLOSED', 'REJECT'),
                require: true,
            },
        },
        {
            sequelize,
            modelName: 'Order',
        },
    );
    return Order;
};
