'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class OrderDetail extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Order, {
                foreignKey: 'orderId',
            });

            this.belongsTo(models.Product, {
                foreignKey: 'productId',
            });
        }
    }
    OrderDetail.init(
        {
            quantity: {
                type: DataTypes.INTEGER,
                require: true,
            },
            unitPrice: {
                type: DataTypes.STRING,
                require: true,
            },
            totalPrice: {
                type: DataTypes.STRING,
                require: true,
            },
        },
        {
            sequelize,
            modelName: 'OrderDetail',
        },
    );
    return OrderDetail;
};
