const createError = require('http-errors');
const { compare } = require('bcryptjs');
const { role } = require('../constants/role.constant');
const apiStatus = require('../constants/apiStatus.constant');
const { generateToken } = require('../utils/jwt.util');
const httpStatus = require('../constants/httpStatus.constant');
const Admin = require('../models').Admin;
const User = require('../models').User;

const signin = async (req, res) => {
    try {
        console.log('Sign in');
        const username = req.body.username;
        const password = req.body.password;

        if (req.body.role === role.ADMIN) {
            const admin = await Admin.findOne({
                where: {
                    username: username,
                },
                raw: true,
            });
            if (!admin) {
                throw createError.Unauthorized('User does not exist');
            }

            const isCorrectPassword = await compare(password, admin.password);
            if (!isCorrectPassword) {
                throw createError.Unauthorized('Incorrect password');
            }

            const userInfo = {
                id: admin.id,
                role: role.ADMIN,
            };

            const token = generateToken(userInfo);
            return res.status(httpStatus.OK).json({
                status: apiStatus.SUCCESS,
                message: 'Login successfully as admin',
                data: {
                    ...admin,
                    password: null,
                    token: token,
                    role: role.ADMIN,
                },
            });
        } else if (req.body.role === role.USER) {
            const user = await User.findOne({
                where: {
                    username: username,
                },
                raw: true,
            });
            if (!user) {
                throw createError.Unauthorized('User does not exist');
            }

            const isCorrectPassword = await compare(password, user.password);
            if (!isCorrectPassword) {
                throw createError.Unauthorized('Incorrect password');
            }

            const userInfo = {
                id: user.id,
                role: role.USER,
            };

            const token = generateToken(userInfo);
            return res.status(httpStatus.OK).json({
                status: apiStatus.SUCCESS,
                message: 'Login successfully as user',
                data: {
                    ...user,
                    password: null,
                    token: token,
                    role: role.USER,
                },
            });
        } else {
            throw createError.Unauthorized('Error occured when user sign in');
        }
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

module.exports = { signin };
