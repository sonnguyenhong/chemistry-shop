const createError = require('http-errors');
const apiStatus = require('../constants/apiStatus.constant');
const httpStatus = require('../constants/httpStatus.constant');
const orderStatus = require('../constants/orderStatus.constant');
const Order = require('../models').Order;
const Product = require('../models').Product;
const User = require('../models').User;
const OrderDetail = require('../models').OrderDetail;
const { generateOrderCode } = require('../utils/generateOrderCode');

const getAll = async (req, res) => {
    try {
        const orders = await Order.findAll({
            include: [{ model: User }],
        });
        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy danh sách đơn hàng thành công',
            data: orders,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const getById = async (req, res) => {
    try {
        const order = await Order.findOne({
            where: {
                id: req.params.id,
            },
            include: [{ model: OrderDetail, include: [{ model: Product }] }],
        });

        if (!order) {
            throw createError.NotFound('Không thể tìm thấy đơn hàng');
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy thông tin đơn hàng thành công',
            data: order,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const create = async (req, res) => {
    try {
        const products = req.body.products;
        const totalPrice = products.reduce((sum, product) => sum + product.quantity * parseInt(product.price), 0);

        const order = await Order.create({
            date: new Date().toISOString(),
            code: generateOrderCode(req.userId),
            totalPrice: totalPrice,
            status: orderStatus.PENDING,
            userId: req.userId,
        });

        const orderId = order.id;

        for (const product of products) {
            await OrderDetail.create({
                quantity: product.quantity,
                unitPrice: product.price,
                totalPrice: parseInt(product.price) * product.quantity,
                orderId: orderId,
                productId: product.id,
            });
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Đặt hàng thành công',
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const update = async (req, res) => {
    try {
        const orderId = req.params.id;
        const status = req.body.status;

        const order = await Order.findOne({
            where: {
                id: orderId
            }
        });

        if(!order) {
            throw createError.NotFound('Không thể tìm thấy đơn hàng');
        }

        const data = {
            status: status
        }

        await Order.update(data, {
            where: {
                id: orderId,
            }
        });

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Sửa thông tin sản phẩm thành công',
        })
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
}

module.exports = { getAll, getById, create, update };
