const createError = require('http-errors');
const { compare } = require('bcryptjs');
const { role } = require('../constants/role.constant');
const apiStatus = require('../constants/apiStatus.constant');
const { generateToken } = require('../utils/jwt.util');
const httpStatus = require('../constants/httpStatus.constant');
const Product = require('../models').Product;

const getAll = async (req, res) => {
    try {
        const products = await Product.findAll();
        if (!products) {
            throw createError.NotFound('Can not get all products');
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Get all products successfully',
            data: products,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const getById = async (req, res) => {
    try {
        const product = await Product.findOne({
            where: {
                id: req.params.id,
            },
        });

        if (!product) {
            throw createError.NotFound(`Fail to find product with id ${req.params.id}`);
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: `Find product with id ${req.params.id} successfully`,
            data: product,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const create = async (req, res) => {
    try {
        const imageUrl = req.file.filename;
        delete req.body.image;
        const data = {
            ...req.body,
            expireDate: new Date(req.body.expireDate),
            importDate: new Date(req.body.importDate),
            imageUrl,
        };

        const product = await Product.create(data);

        if (!product) {
            throw createError.BadRequest('Tạo sản phẩm thất bại');
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Tạo sản phẩm mới thành công',
            data: product,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const remove = async (req, res) => {
    try {
        const id = req.params.id;
        const product = await Product.findOne({
            where: {
                id: id,
            },
        });

        if (!product) {
            throw createError.NotFound('Không tìm thấy sản phẩm');
        }

        await Product.destroy({
            where: {
                id: id,
            },
        });

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Xóa sản phẩm thành công',
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const update = async (req, res) => {
    try {
        const id = req.params.id;
        const product = await Product.findOne({
            where: {
                id: id,
            },
        });

        if (!product) {
            throw createError.NotFound('Không tìm thấy sản phẩm');
        }

        let imageUrl = product.imageUrl;

        if (req.file) {
            delete req.body.image;
            imageUrl = req.file.filename;
        }

        const data = {
            ...req.body,
            expireDate: new Date(req.body.expireDate),
            importDate: new Date(req.body.importDate),
            imageUrl,
        };

        await Product.update(data, {
            where: {
                id: id,
            },
        });

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Sửa thông tin sản phẩm thành công',
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

module.exports = { getAll, getById, create, remove, update };
