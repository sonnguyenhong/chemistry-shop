const createError = require('http-errors');
const apiStatus = require('../constants/apiStatus.constant');
const httpStatus = require('../constants/httpStatus.constant');
const orderStatus = require('../constants/orderStatus.constant');
const Order = require('../models').Order;
const Product = require('../models').Product;
const User = require('../models').User;
const OrderDetail = require('../models').OrderDetail;
const { generateOrderCode } = require('../utils/generateOrderCode');

const getAll = async (req, res) => {
    try {
        const customers = await User.findAll();
        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy danh sách khách hàng thành công',
            data: customers,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const getById = async (req, res) => {
    try {
        const customer = await User.findOne({
            where: {
                id: req.params.id,
            },
        });

        if (!customer) {
            throw createError.NotFound('Không thể tìm thấy khách hàng');
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy thông tin khách hàng thành công',
            data: customer,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const create = async (req, res) => {
    try {
        console.log(req.body);
        const user = await User.create(req.body);
        if(!user) {
            throw createError.BadRequest('Tạo khách hàng mới thất bại');
        }        

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Tạo khách hàng mới thành công',
            data: user,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.OTHER_ERROR,
            message: err.message,
        });
    }
};

const remove = async (req, res) => {
    try {
        const id = req.params.id;
        const customer = await User.findOne({
            where: {
                id: id,
            }
        })

        if(!customer) { 
            throw createError.NotFound('Không tìm thấy khách hàng');
        }

        await User.destroy({
            where: {
                id: id,
            }
        })

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Xóa khách hàng thành công',
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.OTHER_ERROR,
            message: err.message,
        });
    }
}

const update = async (req, res) => {
    try {
        const id = req.params.id;
        const customer = await User.findOne({
            where: {
                id: id,
            }
        })

        if(!customer) { 
            throw createError.NotFound('Không tìm thấy khách hàng');
        }

        await User.update(req.body, {
            where: {
                id: id,
            }
        });

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Sửa thông tin khách hàng thành công',
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.OTHER_ERROR,
            message: err.message,
        });
    }
}

module.exports = { getAll, getById, create, remove, update };
