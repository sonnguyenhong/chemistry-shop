const createError = require('http-errors');
const apiStatus = require('../constants/apiStatus.constant');
const httpStatus = require('../constants/httpStatus.constant');
const orderStatus = require('../constants/orderStatus.constant');
const Order = require('../models').Order;
const Product = require('../models').Product;
const User = require('../models').User;
const OrderDetail = require('../models').OrderDetail;
const Admin = require('../models').Admin;
const { generateOrderCode } = require('../utils/generateOrderCode');

const getAll = async (req, res) => {
    try {
        const employees = await Admin.findAll();
        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy danh sách nhân viên thành công',
            data: employees,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const getById = async (req, res) => {
    try {
        const employee = await User.findOne({
            where: {
                id: req.params.id,
            },
        });

        if (!employee) {
            throw createError.NotFound('Không thể tìm thấy nhân viên');
        }

        return res.status(httpStatus.OK).json({
            status: apiStatus.SUCCESS,
            message: 'Lấy thông tin nhân viên thành công',
            data: employee,
        });
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

const create = async (req, res) => {
    try {
        
    } catch (err) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: apiStatus.AUTH_ERROR,
            message: err.message,
        });
    }
};

module.exports = { getAll, getById, create };
