const generateOrderCode = (userId) => {
    return `${userId}${new Date().getTime()}`;
};

module.exports = { generateOrderCode };
