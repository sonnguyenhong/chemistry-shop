const orderStatus = {
    PENDING: 'PENDING',
    ACCEPT: 'ACCEPT',
    PREPARING: 'PREPARING',
    DELIVERING: 'DELIVERING',
    DELIVERED: 'DELIVERED',
    CLOSED: 'CLOSED',
    REJECT: 'REJECT',
};

module.exports = orderStatus;
