'use strict';
const { hashSync } = require('bcryptjs');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return queryInterface.bulkInsert('Users', [
            {
                username: 'sannhiphutho',
                email: 'sannhiphutho@gmail.com',
                phone: '0967874929',
                password: hashSync('123123', 10),
                name: 'Bệnh viện sản nhi Phú Thọ',
                code: 'KH0001',
                property: 'Bệnh viện nhà nước',
                level: 'Tuyến tỉnh',
                address: 'Đường Trần Phú - Việt Trì - Phú Thọ',
                status: 'ACTIVE',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return queryInterface.bulkDelete('Users', null, {});
    },
};
