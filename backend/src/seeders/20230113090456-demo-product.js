'use strict';

const moment = require('moment');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return queryInterface.bulkInsert('Products', [
            {
                productId: 'HH00000001',
                productCode: '296851',
                englishName: 'Lumipulse G HBsAg-Quant Immunoreaction Cartridges',
                vietnameseName: 'Hóa chất cho máy XN miễn dịch',
                characteristics: '3x14 tests',
                brand: 'Fujirebio',
                origin: 'Nhật Bản',
                serial: 'ABC12345',
                quantity: 2,
                expireDate: moment('31/03/2023', 'DD/MM/YYYY').toDate(),
                importDate: moment('28/12/2022', 'DD/MM/YYYY').toDate(),
                warehouse: 'Kho Đông Á',
                storageCondition: '2 đén 8 độ C',
                price: '1000000',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return queryInterface.bulkDelete('Products', null, {});
    },
};
