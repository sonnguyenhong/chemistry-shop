'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING,
        unique: true,
        require: true,
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
        require: true,
      },
      password: {
        type: Sequelize.STRING,
        require: true,
      },
      phone: {
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      code: {
        type: Sequelize.STRING,
      },
      property: {
        type: Sequelize.STRING,
      },
      level: {
        type: Sequelize.ENUM('Tuyến tỉnh', 'PK tư nhân', 'Tuyến huyện', 'Tuyến Trung ương'),
      },
      address: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.ENUM('PENDING', 'ACTIVE', 'INACTIVE'),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Users');
  }
};