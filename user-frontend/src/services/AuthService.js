import axios from '../configs/axios.config';

const AuthService = {
    login: async (username, password, role) => {
        const response = await axios.post('/api/v1/auth/signin', { username, password, role });

        if (response.data.data && response.data.data.token) {
            localStorage.setItem('user', JSON.stringify(response.data.data));
        }
        return response.data;
    },

    logout: async () => {
        localStorage.removeItem('user');
    },
};

export default AuthService;
