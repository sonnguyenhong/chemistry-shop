import axios from '../configs/axios.config';

const OrderService = {
    getAll: async () => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get('/api/v1/orders', configs);
        return response.data;
    },
    getById: async (id) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get(`/api/v1/orders/${id}`, configs);
        return response.data;
    },
    create: async (products) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const data = { products };

        const response = await axios.post('/api/v1/orders', data, configs);
        return response.data;
    },
};

export default OrderService;
