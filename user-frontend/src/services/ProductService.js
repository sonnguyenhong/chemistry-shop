import axios from '../configs/axios.config';

const ProductService = {
    getAll: async () => {
        const response = await axios.get('/api/v1/products');
        return response.data;
    },

    getById: async (productId) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };
        const response = await axios.get(`/api/v1/products/${productId}`, configs);
        return response.data;
    },
};

export default ProductService;
