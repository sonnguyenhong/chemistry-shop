import { Box, Icon } from '@chakra-ui/react';
import { FaRegCopyright } from 'react-icons/fa';

function Footer() {
    return (
        <Box h="200px" display={'flex'} justifyContent="center" alignItems={'center'} bgColor="white" mt={4}>
            <Icon as={FaRegCopyright} mr={2} />
            Design by Nguyen Hong Son
        </Box>
    );
}

export default Footer;
