import { Box, Menu, MenuButton, MenuItem, MenuList, Text } from '@chakra-ui/react';
import { Icon } from '@chakra-ui/icons';
import { FaShoppingCart, FaUserAlt } from 'react-icons/fa';
import { Link, useNavigate } from 'react-router-dom';
import store from '../../store/store';
import { connect, useDispatch } from 'react-redux';
import { logout } from '../../redux/action_creators/AuthActions';

function Header(props) {
    const { user } = store.getState().authReducer;

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleLogout = (e) => {
        e.preventDefault();
        dispatch(logout());
        navigate('/');
    };

    const handleClickOrderList = (e) => {
        e.preventDefault();
        navigate('/orders');
    };

    return (
        <Box
            height="80px"
            bgColor={'#189eff'}
            display="flex"
            pl={100}
            pr={100}
            alignItems="center"
            justifyContent="space-between"
        >
            <Box display={'flex'} alignItems="center" cursor={'pointer'}>
                <Link to="/">
                    <Text fontSize={45} fontWeight="600" color={'white'}>
                        AMV
                    </Text>
                </Link>
            </Box>
            <Box display={'flex'} alignItems="center">
                <Link to="/cart" className="mr-16" cursor="pointer" textDecoration={'none'}>
                    <Icon as={FaShoppingCart} boxSize={25} color="white" />
                    <Text fontSize={12} color="white">
                        Giỏ hàng {user ? `(${props.numberCart})` : ''}
                    </Text>
                </Link>
                {user ? (
                    <Menu>
                        <MenuButton>
                            <Icon as={FaUserAlt} boxSize={25} color="white" />
                            <Text fontSize={12} color="white">
                                {user.name}
                            </Text>
                        </MenuButton>
                        <MenuList>
                            <MenuItem>
                                <Text>Hồ sơ cá nhân</Text>
                            </MenuItem>
                            <MenuItem onClick={handleClickOrderList}>
                                <Text>Danh sách đơn hàng</Text>
                            </MenuItem>
                            <MenuItem onClick={handleLogout}>
                                <Text>Đăng xuất</Text>
                            </MenuItem>
                        </MenuList>
                    </Menu>
                ) : (
                    <Link to="/login" cursor="pointer">
                        <Icon as={FaUserAlt} boxSize={25} color="white" />
                        <Text fontSize={12} color="white">
                            Đăng nhập
                        </Text>
                    </Link>
                )}
            </Box>
        </Box>
    );
}

const mapStateToProps = (state) => {
    return {
        numberCart: state.productReducer.numberCart,
    };
};

export default connect(mapStateToProps, null)(Header);
