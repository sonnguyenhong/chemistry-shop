import { Box, Image, Text } from '@chakra-ui/react';
import { separatePrice } from '../../utils/MoneyUtil';

function Product(props) {
    return (
        <Box
            display={'flex'}
            flexDir="column"
            maxW={200}
            bgColor="white"
            p={4}
            borderRadius={10}
            cursor="pointer"
            onClick={props.onClick}
        >
            <Image src="https://bit.ly/dan-abramov" alt="Dan Abramov" borderRadius={10} />
            <Text fontSize={14} fontWeight="500">
                {props.data.vietnameseName}
            </Text>
            <Text fontSize={14}>{separatePrice(props.data.price)} VNĐ</Text>
        </Box>
    );
}

export default Product;
