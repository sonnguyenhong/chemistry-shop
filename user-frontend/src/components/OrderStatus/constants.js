import OrderStatus from '../../constants/OrderStatus';

export const OrderStatusStyle = {
    [OrderStatus.PENDING]: {
        color: '#FFC107',
        backgroundColor: '#FFE082',
    },
    [OrderStatus.ACCEPT]: {
        color: '#8BC34A',
        backgroundColor: '#C8E6C9',
    },
    [OrderStatus.PREPARING]: {
        color: '#00BCD4',
        backgroundColor: '#B2EBF2',
    },
    [OrderStatus.DELIVERING]: {
        color: '#795548',
        backgroundColor: '#FFC107',
    },
    [OrderStatus.DELIVERED]: {
        color: '#00C853',
        backgroundColor: '#C8E6C9',
    },
    [OrderStatus.CLOSED]: {
        color: '#616161 ',
        backgroundColor: '#BDBDBD ',
    },
    [OrderStatus.REJECT]: {
        color: '#F44336',
        backgroundColor: '#FFCDD2',
    },
};
