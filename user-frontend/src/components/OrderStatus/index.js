import { Box } from '@chakra-ui/react';
import { OrderStatusStyle } from './constants';

function OrderStatus(props) {
    return (
        <Box
            color={OrderStatusStyle[props.status].color}
            backgroundColor={OrderStatusStyle[props.status].backgroundColor}
            padding={2}
            borderRadius={4}
            textAlign={'center'}
            fontWeight={'500'}
        >
            {props.status}
        </Box>
    );
}

export default OrderStatus;
