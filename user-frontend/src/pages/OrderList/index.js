import { Box, Button, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr, useToast } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import OrderService from '../../services/OrderService';
import ApiStatus from '../../constants/ApiStatus';
import { useNavigate } from 'react-router-dom';
import { convertToDayMonthYear } from '../../utils/DateUtil';
import { separatePrice } from '../../utils/MoneyUtil';
import OrderStatus from '../../components/OrderStatus';
function OrderList(props) {
    const [orders, setOrders] = useState([]);
    const toast = useToast();
    const navigate = useNavigate();

    useEffect(() => {
        OrderService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setOrders(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast]);

    const handleDetailOrder = (orderId) => {
        navigate(`/orders/${orderId}`);
    };

    return (
        <Box>
            <Box>
                <TableContainer>
                    <Table variant="simple">
                        <Thead>
                            <Tr>
                                <Th>STT</Th>
                                <Th>Mã đơn hàng</Th>
                                <Th>Ngày đặt hàng</Th>
                                <Th>Tổng giá tiền (VNĐ)</Th>
                                <Th>Trạng thái</Th>
                                <Th>Thao tác</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {orders.length <= 0 && (
                                <Tr mt={4}>
                                    <Td colSpan={5} textAlign={'center'} fontWeight={'450'}>
                                        Bạn chưa có đơn hàng nào
                                    </Td>
                                </Tr>
                            )}
                            {orders.map((order, index) => {
                                return (
                                    <Tr key={index}>
                                        <Td>{index + 1}</Td>
                                        <Td>{order.code}</Td>
                                        <Td>{convertToDayMonthYear(order.createdAt)}</Td>
                                        <Td>{separatePrice(order.totalPrice)}</Td>
                                        <Td>
                                            <OrderStatus status={order.status} />
                                        </Td>
                                        <Td>
                                            <Button
                                                bgColor={'#189eff'}
                                                _hover={{ opacity: 0.7 }}
                                                onClick={(e) => {
                                                    e.preventDefault();
                                                    handleDetailOrder(order.id);
                                                }}
                                            >
                                                <Text color={'white'}>Chi tiết</Text>
                                            </Button>
                                        </Td>
                                    </Tr>
                                );
                            })}
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    );
}

export default OrderList;
