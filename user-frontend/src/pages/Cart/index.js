import {
    Box,
    Button,
    Image,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    useToast,
} from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
import { connect, useDispatch } from 'react-redux';
import { deleteAllItems, deleteCart } from '../../redux/action_creators/ProductActions';
import { useState } from 'react';
import OrderService from '../../services/OrderService';
import ApiStatus from '../../constants/ApiStatus';
import { useNavigate } from 'react-router-dom';

function Cart(props) {
    const [isOpenDeleteCartModal, setIsOpenDeleteCartModal] = useState(false);
    const [isOpenOrderModal, setIsOpenOrderModal] = useState(false);
    const [selectedCartIndex, setSelectedCartIndex] = useState();
    const dispatch = useDispatch();
    const toast = useToast();
    const navigate = useNavigate();

    const handleDeleteCart = (e) => {
        e.preventDefault();
        dispatch(deleteCart(selectedCartIndex));
        setIsOpenDeleteCartModal(false);
    };

    const handleSubmitOrder = (e) => {
        e.preventDefault();

        if (props.carts.length <= 0) {
            toast({
                title: 'Chưa có vật phẩm trong giỏ hàng',
                status: 'error',
                position: 'bottom',
                duration: 5000,
                isClosable: true,
            });
        } else {
            OrderService.create(props.carts)
                .then((response) => {
                    if (response.status !== ApiStatus.SUCCESS) {
                        toast({
                            title: response.message,
                            status: 'error',
                            position: 'bottom',
                            duration: 5000,
                            isClosable: true,
                        });
                    } else {
                        toast({
                            title: response.message,
                            status: 'success',
                            position: 'bottom',
                            duration: 5000,
                            isClosable: true,
                        });
                        dispatch(deleteAllItems());
                        navigate('/orders');
                    }
                })
                .catch((err) => {
                    toast({
                        title: err.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                });
        }
    };

    return (
        <Box display="flex" flex={1}>
            {/* Modal cho xóa cart item  */}
            <Modal onClose={() => setIsOpenDeleteCartModal(false)} isOpen={isOpenDeleteCartModal} isCentered>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Xóa mặt hàng</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Text>Bạn có chắc chắn muốn xóa mặt hàng khỏi đơn hàng</Text>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            onClick={handleDeleteCart}
                            bgColor={'red'}
                            color={'white'}
                            mr={4}
                            _hover={{ opacity: 0.7 }}
                        >
                            Xác nhận xóa
                        </Button>
                        <Button
                            onClick={() => setIsOpenDeleteCartModal(false)}
                            bgColor={'#189eff'}
                            color={'white'}
                            _hover={{ opacity: 0.7 }}
                        >
                            Hoàn tác
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>

            {/* Modal cho đặt hàng  */}
            <Modal onClose={() => setIsOpenOrderModal(false)} isOpen={isOpenOrderModal} isCentered>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Đặt hàng</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Text>Bạn hãy xác nhận đặt hàng</Text>
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            onClick={handleSubmitOrder}
                            bgColor={'#189eff'}
                            color={'white'}
                            mr={4}
                            _hover={{ opacity: 0.7 }}
                        >
                            Xác nhận
                        </Button>
                        <Button
                            onClick={() => setIsOpenOrderModal(false)}
                            bgColor={'#ddd'}
                            color={'#000'}
                            _hover={{ opacity: 0.7 }}
                        >
                            Hoàn tác
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>

            {/* Table  */}
            <Box bgColor={'white'} flex={3} mr={4}>
                <TableContainer>
                    <Table variant="simple">
                        <Thead>
                            <Tr>
                                <Th>Sản phẩm</Th>
                                <Th>Đơn giá (VNĐ)</Th>
                                <Th>Số lượng</Th>
                                <Th>Tổng</Th>
                                <Th>Thao tác</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {props.carts.length <= 0 && (
                                <Tr mt={4}>
                                    <Td colSpan={5} textAlign={'center'} fontWeight={'450'}>
                                        Chưa có sản phẩm trong giỏ hàng
                                    </Td>
                                </Tr>
                            )}
                            {props.carts.map((cartItem, index) => {
                                return (
                                    <Tr key={index}>
                                        <Td display={'flex'} alignItems="center">
                                            <Image
                                                src="https://bit.ly/dan-abramov"
                                                alt="Dan Abramov"
                                                borderRadius={10}
                                                boxSize={20}
                                                mr={4}
                                            />
                                            <Text fontSize={14} fontWeight="500">
                                                {cartItem.name}
                                            </Text>
                                        </Td>
                                        <Td>{cartItem.price}</Td>
                                        <Td>
                                            <NumberInput w={100} value={cartItem.quantity}>
                                                <NumberInputField />
                                                <NumberInputStepper>
                                                    <NumberIncrementStepper />
                                                    <NumberDecrementStepper />
                                                </NumberInputStepper>
                                            </NumberInput>
                                        </Td>
                                        <Td>{parseInt(cartItem.price) * cartItem.quantity}</Td>
                                        <Td>
                                            <Button
                                                bgColor={'red'}
                                                _hover={{ opacity: 0.7 }}
                                                onClick={() => {
                                                    setSelectedCartIndex(index);
                                                    setIsOpenDeleteCartModal(true);
                                                }}
                                            >
                                                <DeleteIcon color={'white'} />
                                            </Button>
                                        </Td>
                                    </Tr>
                                );
                            })}
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>

            {/* Price  */}
            <Box flex={1} bgColor="#f5f5f5" p={4} borderRadius={5}>
                <Text fontSize={20} fontWeight="500">
                    Thông tin
                </Text>
                <Text mt={4}>
                    <b>Tổng giá tiền:</b>{' '}
                    {props.carts.reduce(
                        (sum, cartItem) => sum + parseInt(cartItem.price) * parseInt(cartItem.quantity),
                        0,
                    )}{' '}
                    VNĐ
                </Text>
                <Button
                    colorScheme={'blue'}
                    mt={8}
                    onClick={() => {
                        if (props.carts.length <= 0) {
                            toast({
                                title: 'Chưa có vật phẩm trong giỏ hàng',
                                status: 'error',
                                position: 'bottom',
                                duration: 5000,
                                isClosable: true,
                            });
                        } else {
                            setIsOpenOrderModal(true);
                        }
                    }}
                >
                    ĐẶT HÀNG NGAY
                </Button>
            </Box>
        </Box>
    );
}

const mapStateToProps = (state) => {
    return {
        carts: state.productReducer.Carts,
    };
};

export default connect(mapStateToProps, null)(Cart);
