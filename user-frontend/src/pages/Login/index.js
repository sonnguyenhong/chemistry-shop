import { Box, Button, FormControl, FormLabel, Input, Text, useToast } from '@chakra-ui/react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import UserRole from '../../constants/UserRole';
import { login } from '../../redux/action_creators/AuthActions';
import store from '../../store/store';
import { useNavigate } from 'react-router-dom';

function Login() {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [isLoadingLogin, setIsLoadingLogin] = useState(false);

    const toast = useToast();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleLogin = (e) => {
        e.preventDefault();
        setIsLoadingLogin(true);
        dispatch(login(username, password, UserRole.USER)).then(() => {
            if (store.getState().authReducer.isLoggedIn) {
                setIsLoadingLogin(false);
                navigate('/');
            } else {
                toast({
                    title: store.getState().authReducer.error.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
                setIsLoadingLogin(false);
            }
        });
    };

    return (
        <Box bgColor={'white'} w={500} borderRadius={10} padding={8} boxShadow="lg" p="6" rounded="md" bg="white">
            <Text fontSize={25} fontWeight="600">
                ĐĂNG NHẬP
            </Text>
            <FormControl isRequired>
                <FormLabel>Username</FormLabel>
                <Input onChange={(e) => setUsername(e.target.value)} placeholder="Enter your username" />
            </FormControl>
            <FormControl isRequired>
                <FormLabel>Password</FormLabel>
                <Input
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Enter your password"
                    type="password"
                />
            </FormControl>
            <Button onClick={handleLogin} mt={4} colorScheme="blue" isLoading={isLoadingLogin}>
                Đăng nhập
            </Button>
        </Box>
    );
}

export default Login;
