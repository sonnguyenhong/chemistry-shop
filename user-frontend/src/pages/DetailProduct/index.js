import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Image,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Spinner,
    Text,
    useToast,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ProductService from '../../services/ProductService';
import ApiStatus from '../../constants/ApiStatus';
import { useDispatch } from 'react-redux';
import { addCart } from '../../redux/action_creators/ProductActions';

function DetailProduct() {
    const [product, setProduct] = useState();
    const [addedQuantity, setAddedQuantity] = useState(0);
    const { id } = useParams();
    const toast = useToast();
    const dispatch = useDispatch();

    useEffect(() => {
        ProductService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setProduct(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [id, toast]);

    const handleAddToCart = (e) => {
        e.preventDefault();
        if (addedQuantity <= 0) {
            toast({
                title: 'Bạn hãy chọn số lượng muốn mua',
                status: 'error',
                position: 'bottom',
                duration: 5000,
                isClosable: true,
            });
        } else {
            dispatch(addCart({ ...product, addedQuantity }));
            toast({
                title: 'Thêm vào giỏ hàng thành công',
                status: 'success',
                position: 'bottom',
                duration: 5000,
                isClosable: true,
            });
        }
    };

    if (!product) {
        return (
            <Box w={'100%'} h={'100%'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <Spinner />
            </Box>
        );
    } else {
        return (
            <Box pl={100} pr={100} display={'flex'} w={'100%'}>
                {/* Image  */}
                <Box w={'50%'}>
                    <Image w={'80%'} maxW={500} src="https://bit.ly/dan-abramov" alt="Dan Abramov" borderRadius={5} />
                </Box>

                {/* Description and add to cart */}
                <Box
                    bgColor={'white'}
                    borderRadius={5}
                    w={'50%'}
                    p={4}
                    display={'flex'}
                    flexDir={'column'}
                    justifyContent={'space-between'}
                >
                    <Box mb={4}>
                        <Text fontSize={18} fontWeight={'500'}>
                            {product.vietnameseName}
                        </Text>
                        <Box mt={4} textAlign={'left'}>
                            <Text>
                                <b>Mã sản phẩm:</b> {product.productCode}
                            </Text>
                            <Text>
                                <b>Hãng sản xuất:</b> {product.brand}
                            </Text>
                            <Text>
                                <b>Xuất xứ:</b> {product.origin}
                            </Text>
                            <Text>
                                <b>Ngày nhập:</b> {product.importDate}
                            </Text>
                            <Text>
                                <b>Hạn sử dụng:</b> {product.expireDate}
                            </Text>
                            <Text>
                                <b>Điều kiện bảo quản:</b> {product.storageCondition}
                            </Text>
                            <Text>
                                <b>Số lượng còn lại:</b> {product.quantity}
                            </Text>
                            <Text>
                                <b>Đơn giá:</b> {product.price} VNĐ
                            </Text>
                        </Box>
                    </Box>
                    <Box>
                        <FormControl mb={4}>
                            <FormLabel>Số lượng</FormLabel>
                            <NumberInput
                                min={0}
                                value={addedQuantity}
                                onChange={(value) => setAddedQuantity(parseInt(value))}
                            >
                                <NumberInputField />
                                <NumberInputStepper>
                                    <NumberIncrementStepper />
                                    <NumberDecrementStepper />
                                </NumberInputStepper>
                            </NumberInput>
                        </FormControl>
                        <Button bgColor={'#189eff'} color={'white'} _hover={{ opacity: 0.9 }} onClick={handleAddToCart}>
                            Thêm vào giỏ hàng
                        </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}

export default DetailProduct;
