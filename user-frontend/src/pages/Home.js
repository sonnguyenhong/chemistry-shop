import { Box, Grid, Select, Text, useToast } from '@chakra-ui/react';
import Product from '../components/Product';
import { useEffect } from 'react';
import ProductService from '../services/ProductService';
import ApiStatus from '../constants/ApiStatus';
import { useNavigate } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { getAllProduct } from '../redux/action_creators/ProductActions';

function Home(props) {
    const toast = useToast();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        ProductService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    dispatch(getAllProduct(response.data));
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast, dispatch]);

    const handleClickProduct = (productId) => {
        navigate(`/products/${productId}`);
    };

    return (
        <Box w="100%" mr={20}>
            <Box bgColor={'white'} display="flex" mb={4} alignItems="center" p={4}>
                <Text mr={4}>Sắp xếp</Text>
                <Select placeholder="Thứ tự" w={150}>
                    <option value={'Giá'}>Giá</option>
                    <option value={'Tên'}>Tên</option>
                </Select>
            </Box>
            <Grid templateColumns="repeat(5, 1fr)" gap={3}>
                {props.products.map((product, index) => {
                    return <Product key={index} data={product} onClick={() => handleClickProduct(product.id)} />;
                })}
            </Grid>
        </Box>
    );
}

const mapStateToProps = (state) => {
    return {
        products: state.productReducer.Products,
    };
};

export default connect(mapStateToProps, null)(Home);
