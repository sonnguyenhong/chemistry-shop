import {
    Box,
    Image,
    Spinner,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Tfoot,
    Th,
    Thead,
    Tr,
    useToast,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import OrderService from '../../services/OrderService';
import ApiStatus from '../../constants/ApiStatus';
import { separatePrice } from '../../utils/MoneyUtil';
import OrderStatus from '../../components/OrderStatus';
function OrderDetail() {
    const [order, setOrder] = useState();
    const { id } = useParams(); // id cua don hang
    const toast = useToast();

    useEffect(() => {
        OrderService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setOrder(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast, id]);

    if (!order) {
        <Box>
            <Spinner />
        </Box>;
    } else {
        return (
            <Box>
                <Box mb={4}>
                    <Text fontSize={20}>Đơn hàng {order.code}</Text>
                    <Box display={'flex'} alignItems={'center'} justifyContent={'center'}>
                        <Text fontSize={18} mr={2}>
                            Trạng thái đơn hàng:{' '}
                        </Text>
                        <OrderStatus status={order.status} />
                    </Box>
                </Box>
                <Box>
                    <TableContainer>
                        <Table variant="simple">
                            <Thead>
                                <Tr>
                                    <Th>Tên mặt hàng</Th>
                                    <Th>Đơn giá (VNĐ)</Th>
                                    <Th>Số lượng</Th>
                                    <Th>Thành tiền (VNĐ)</Th>
                                </Tr>
                            </Thead>
                            <Tbody>
                                {order.OrderDetails.length <= 0 && (
                                    <Tr mt={4}>
                                        <Td colSpan={4} textAlign={'center'} fontWeight={'450'}>
                                            Không có sản phẩm
                                        </Td>
                                    </Tr>
                                )}
                                {order.OrderDetails.map((orderDetail, index) => {
                                    return (
                                        <Tr key={index}>
                                            <Td display={'flex'} alignItems={'center'}>
                                                <Image
                                                    src="https://bit.ly/dan-abramov"
                                                    alt="Dan Abramov"
                                                    borderRadius={10}
                                                    boxSize={20}
                                                    mr={4}
                                                />
                                                <Text>{orderDetail.Product.vietnameseName}</Text>
                                            </Td>
                                            <Td>{separatePrice(orderDetail.Product.price)}</Td>
                                            <Td>{orderDetail.quantity}</Td>
                                            <Td>
                                                {separatePrice(
                                                    (
                                                        parseInt(orderDetail.Product.price) * orderDetail.quantity
                                                    ).toString(),
                                                )}
                                            </Td>
                                        </Tr>
                                    );
                                })}
                            </Tbody>
                            <Tfoot>
                                <Tr>
                                    <Th fontSize={20} textAlign={'center'} colSpan={3}>
                                        Tổng giá tiền
                                    </Th>
                                    <Th fontSize={20}>{separatePrice(order.totalPrice)}</Th>
                                </Tr>
                            </Tfoot>
                        </Table>
                    </TableContainer>
                </Box>
            </Box>
        );
    }
}

export default OrderDetail;
