import { Box } from '@chakra-ui/react';
import Header from '../components/Header';
import Footer from '../components/Footer';

function DefaultLayout({ children }) {
    return (
        <Box>
            <Header />
            <Box mt={10} mb={10} minH={400} w={'100%'}>
                {children}
            </Box>
            <Footer />
        </Box>
    );
}

export default DefaultLayout;
