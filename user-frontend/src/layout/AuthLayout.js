import { Box } from '@chakra-ui/react';
import Header from '../components/Header';
import Footer from '../components/Footer';

function AuthLayout({ children }) {
    return (
        <Box>
            <Header />
            <Box display={'flex'} justifyContent="center" alignItems={'center'} mt={10} mb={10} h={400}>
                {children}
            </Box>
            <Footer />
        </Box>
    );
}

export default AuthLayout;
