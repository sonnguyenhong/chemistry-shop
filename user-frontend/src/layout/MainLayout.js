import { Box, Link, Text, VStack, Icon } from '@chakra-ui/react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { FaGreaterThan } from 'react-icons/fa';

function MainLayout({ children }) {
    return (
        <Box>
            <Header />
            <Box mt={4} minH={500} w="100%" display={'flex'}>
                {/* SideBar */}
                <Box w="25%" h={'500px'} mr={4} ml={20} bgColor="white" borderRadius={10}>
                    <VStack>
                        <Text fontSize={18} textAlign="left" pl={4} fontWeight="700" mt={2} w="100%">
                            DANH MỤC HÓA CHẤT
                        </Text>
                        <Box w="100%" textAlign={'left'} pl={4}>
                            <Text fontSize={18} fontWeight="600">
                                HÃNG SẢN XUẤT
                            </Text>
                            <Box display={'flex'} flexDir="column">
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Fujirebio
                                </Link>
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Fujirebio
                                </Link>
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Fujirebio
                                </Link>
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Fujirebio
                                </Link>
                            </Box>
                        </Box>
                        <Box w="100%" textAlign={'left'} pl={4}>
                            <Text fontSize={18} fontWeight="600">
                                XUẤT XỨ
                            </Text>
                            <Box display={'flex'} flexDir="column">
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Nhật Bản
                                </Link>
                                <Link>
                                    <Icon as={FaGreaterThan} boxSize={3} mr={2} />
                                    Trung Quốc
                                </Link>
                            </Box>
                        </Box>
                    </VStack>
                </Box>
                {children}
            </Box>
            <Footer />
        </Box>
    );
}

export default MainLayout;
