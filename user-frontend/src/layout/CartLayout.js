import { Box, Text } from '@chakra-ui/react';
import Header from '../components/Header';
import Footer from '../components/Footer';

function CartLayout({ children }) {
    return (
        <Box>
            <Header />
            <Box mt={10} mb={10} minH={400}>
                <Text ml={20} mb={4} textAlign={'left'} fontSize={22} fontWeight="500">
                    Giỏ hàng
                </Text>
                <Box bgColor={'white'} ml={20} mr={20} boxShadow="lg" p="6" rounded="md" bg="white">
                    {children}
                </Box>
            </Box>
            <Footer />
        </Box>
    );
}

export default CartLayout;
