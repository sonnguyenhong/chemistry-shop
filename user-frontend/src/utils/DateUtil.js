export const convertToDayMonthYear = (dateString) => {
    const date = new Date(dateString);
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    if (day.toString().length <= 1) {
        day = `0${day}`;
    }
    if (month.toString().length <= 1) {
        month = `0${month}`;
    }
    return `${day}/${month}/${year}`;
};
