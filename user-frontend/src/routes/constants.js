import AuthLayout from '../layout/AuthLayout';
import CartLayout from '../layout/CartLayout';
import MainLayout from '../layout/MainLayout';
import DefaultLayout from '../layout/DefaultLayout';
import Cart from '../pages/Cart';
import DetailProduct from '../pages/DetailProduct';
import Home from '../pages/Home';
import Login from '../pages/Login';
import OrderList from '../pages/OrderList';
import OrderDetail from '../pages/OrderDetail';
import OrderLayout from '../layout/OrderLayout';

export const publicUrl = [
    {
        path: '/login',
        element: Login,
        layout: AuthLayout,
    },
    {
        path: '/',
        element: Home,
        layout: MainLayout,
    },
];

export const privateUrl = [
    {
        path: '/cart',
        element: Cart,
        layout: CartLayout,
    },
    {
        path: '/products/:id',
        element: DetailProduct,
        layout: DefaultLayout,
    },
    {
        path: '/orders',
        element: OrderList,
        layout: OrderLayout,
    },
    {
        path: '/orders/:id',
        element: OrderDetail,
        layout: OrderLayout,
    },
];
