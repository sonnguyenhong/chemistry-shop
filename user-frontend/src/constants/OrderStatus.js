const OrderStatus = {
    PENDING: 'PENDING',
    ACCEPT: 'ACCEPT',
    PREPARING: 'PREPARING',
    DELIVERING: 'DELIVERING',
    DELIVERED: 'DELIVERED',
    CLOSED: 'CLOSED',
    REJECT: 'REJECT',
};

export default OrderStatus;
