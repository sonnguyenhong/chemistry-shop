import { combineReducers } from 'redux';

import authReducer from './AuthReducer';
import productReducer from './ProductReducer';

const reducer = combineReducers({
    authReducer,
    productReducer,
});

export default reducer;
