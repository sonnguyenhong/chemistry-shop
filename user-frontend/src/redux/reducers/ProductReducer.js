import {
    ADD_CART,
    DECREASE_QUANTITY,
    DELETE_ALL_ITEMS,
    DELETE_CART,
    GET_ALL_PRODUCT,
    GET_NUMBER_CART,
    INCREASE_QUANTITY,
} from '../actions/actions';

const initState = {
    numberCart: 0, // Số lượng sản phẩm đã mua có trong giỏ hàng
    Carts: [], // Mảng giỏ hàng đầu tiên rỗng
    Products: [], // Tất cả sản phẩm được lấy từ API
};

const productReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_ALL_PRODUCT:
            return {
                ...state,
                Products: action.payload,
            };

        case INCREASE_QUANTITY:
            state.numberCart++;
            state.Carts[action.payload].quantity++;
            return {
                ...state,
            };

        case DECREASE_QUANTITY:
            let quantity = state.Carts[action.payload].quantity;
            if (quantity > 1) {
                state.numberCart--;
                state.Carts[action.payload].quantity--;
            }
            return {
                ...state,
            };

        case GET_NUMBER_CART:
            return {
                ...state,
            };

        case ADD_CART:
            if (state.numberCart === 0) {
                let cart = {
                    id: action.payload.id,
                    quantity: action.payload.addedQuantity,
                    name: action.payload.vietnameseName,
                    imageUrl: action.payload.imageUrl,
                    price: action.payload.price,
                };

                state.Carts.push(cart);
            } else {
                let check = false;
                state.Carts.map((cartItem, index) => {
                    if (cartItem.id === action.payload.id) {
                        state.Carts[index].quantity += action.payload.addedQuantity;
                        check = true;
                    }
                });

                if (!check) {
                    let _cart = {
                        id: action.payload.id,
                        quantity: action.payload.addedQuantity,
                        name: action.payload.vietnameseName,
                        imageUrl: action.payload.imageUrl,
                        price: action.payload.price,
                    };
                    state.Carts.push(_cart);
                }
            }

            return {
                ...state,
                numberCart: state.numberCart + action.payload.addedQuantity,
            };

        case DELETE_CART:
            let _quantity = state.Carts[action.payload].quantity;
            return {
                ...state,
                numberCart: state.numberCart - _quantity,
                Carts: state.Carts.filter((cartItem) => {
                    return cartItem.id !== state.Carts[action.payload].id;
                }),
            };

        case DELETE_ALL_ITEMS:
            return {
                ...state,
                numberCart: 0,
                Carts: [],
            };

        default:
            return state;
    }
};

export default productReducer;
