import {
    ADD_CART,
    DECREASE_QUANTITY,
    DELETE_ALL_ITEMS,
    DELETE_CART,
    GET_ALL_PRODUCT,
    GET_NUMBER_CART,
    INCREASE_QUANTITY,
    UPDATE_CART,
} from '../actions/actions';

export const increaseQuantity = (payload) => {
    return {
        type: INCREASE_QUANTITY,
        payload,
    };
};

export const decreaseQuantity = (payload) => {
    return {
        type: DECREASE_QUANTITY,
        payload,
    };
};

export const getAllProduct = (payload) => {
    return {
        type: GET_ALL_PRODUCT,
        payload,
    };
};

export const getNumberCart = () => {
    return {
        type: GET_NUMBER_CART,
    };
};

export const addCart = (payload) => {
    return {
        type: ADD_CART,
        payload,
    };
};

export const updateCart = (payload) => {
    return {
        type: UPDATE_CART,
        payload,
    };
};

export const deleteCart = (payload) => {
    return {
        type: DELETE_CART,
        payload,
    };
};

export const deleteAllItems = () => {
    return {
        type: DELETE_ALL_ITEMS,
    };
};
