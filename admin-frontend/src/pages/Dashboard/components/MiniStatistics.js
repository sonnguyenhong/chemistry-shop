import { Card, CardBody, Flex, Stat, StatLabel, StatNumber } from '@chakra-ui/react';
import { separatePrice } from '../../../utils/MoneyUtil';

import React from 'react';
import { StatisticsType } from '../../../constants/StatisticsType';

const MiniStatistics = ({ title, amount, backgroundColor, color, minH, type }) => {
    return (
        <Card minH={minH ?? ''} width={'100%'} bgColor={backgroundColor ?? 'white'}>
            <CardBody>
                <Flex flexDirection="row" align="center" justify="center" w="100%">
                    <Stat me="auto">
                        <StatLabel
                            fontSize="md"
                            color={type === StatisticsType.ORDER ? '#999' : 'white'}
                            pb=".1rem"
                            textAlign={'center'}
                        >
                            {title}
                        </StatLabel>
                        <Flex justify={'center'}>
                            {type === StatisticsType.ORDER ? (
                                <StatNumber fontSize={30} color={color ?? 'white'}>
                                    {amount}
                                </StatNumber>
                            ) : (
                                <StatNumber fontSize={45} color={color ?? 'white'}>
                                    {separatePrice(amount.toString())} VNĐ
                                </StatNumber>
                            )}
                        </Flex>
                    </Stat>
                </Flex>
            </CardBody>
        </Card>
    );
};

export default MiniStatistics;
