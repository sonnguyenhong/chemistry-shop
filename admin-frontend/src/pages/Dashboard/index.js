import { Box, Grid, HStack, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr, useToast } from '@chakra-ui/react';
import MiniStatisTics from './components/MiniStatistics';
import { StatisticsType } from '../../constants/StatisticsType';
import OrderStatusConstant from '../../constants/OrderStatus';
import OrderStatus from '../../components/OrderStatus';
import { useEffect, useState } from 'react';
import OrderService from '../../services/OrderService';
import ApiStatus from '../../constants/ApiStatus';
import { convertToDayMonthYear } from '../../utils/DateUtil';
import { separatePrice } from '../../utils/MoneyUtil';

function Dashboard() {
    const [orders, setOrders] = useState([]);
    const [todayOrderPrice, setTodayOrderPrice] = useState('0');
    const [monthOrderPrice, setMonthOrderPrice] = useState('0');
    const [totalPrice, setTotalPrice] = useState('0');
    const [totalOrders, setTotalOrders] = useState('0');
    const [totalPendingOrders, setTotalPendingOrders] = useState('0');
    const [totalAcceptedOrders, setTotalAcceptedOrders] = useState('0');
    const [totalPreparingOrders, setTotalPreparingOrders] = useState('0');
    const [totalDeliveringOrders, setTotalDeliveringOrders] = useState('0');
    const [totalDeliveredOrders, setTotalDeliveredOrders] = useState('0');
    const [totalClosedOrders, setTotalClosedOrders] = useState('0');
    const [totalRejectedOrders, setTotalRejectedOrders] = useState('0');

    const miniStatistics = [
        {
            type: StatisticsType.MONEY,
            title: 'Tổng giá trị đơn hàng hôm nay',
            amount: todayOrderPrice,
            backgroundColor: '#0694a2',
        },
        {
            type: StatisticsType.MONEY,
            title: 'Tổng giá trị đơn hàng tháng này',
            amount: monthOrderPrice,
            backgroundColor: '#3f83f8',
        },
        {
            type: StatisticsType.MONEY,
            title: 'Tổng giá trị',
            amount: totalPrice,
            backgroundColor: '#0e9f6e',
        },
    ];

    const orderStatistics = [
        {
            type: StatisticsType.ORDER,
            title: 'Tổng đơn hàng',
            amount: totalOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đang chờ',
            amount: totalPendingOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đã nhận',
            amount: totalAcceptedOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đang chuẩn bị',
            amount: totalPreparingOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đang giao',
            amount: totalDeliveringOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đã giao thành công',
            amount: totalDeliveredOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đã đóng',
            amount: totalClosedOrders,
            color: 'black',
        },
        {
            type: StatisticsType.ORDER,
            title: 'Đơn hàng đã hủy',
            amount: totalRejectedOrders,
            color: 'black',
        },
    ];

    const toast = useToast();

    useEffect(() => {
        OrderService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    let fetchedOrders = response.data;
                    setOrders(fetchedOrders);
                    setTotalOrders(`${fetchedOrders.length}`);
                    const currentDate = new Date().getDate();
                    const currentMonth = new Date().getMonth();
                    const currentYear = new Date().getFullYear();

                    for (const order of fetchedOrders) {
                        const orderDate = new Date(order.date).getDate();
                        const orderMonth = new Date(order.date).getMonth();
                        const orderYear = new Date(order.date).getFullYear();
                        setTotalPrice(`${parseInt(totalPrice) + parseInt(order.totalPrice)}`);
                        if (order.status === OrderStatusConstant.PENDING) {
                            setTotalPendingOrders(`${parseInt(totalPendingOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.ACCEPT) {
                            setTotalAcceptedOrders(`${parseInt(totalAcceptedOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.PREPARING) {
                            setTotalPreparingOrders(`${parseInt(totalPreparingOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.DELIVERING) {
                            setTotalDeliveringOrders(`${parseInt(totalDeliveringOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.DELIVERED) {
                            setTotalDeliveredOrders(`${parseInt(totalDeliveredOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.CLOSED) {
                            setTotalClosedOrders(`${parseInt(totalClosedOrders) + 1}`);
                        } else if (order.status === OrderStatusConstant.REJECT) {
                            setTotalRejectedOrders(`${parseInt(totalRejectedOrders) + 1}`);
                        }
                        if (orderDate === currentDate && orderMonth === currentMonth && orderYear === currentYear) {
                            setTodayOrderPrice(`${parseInt(todayOrderPrice) + parseInt(order.totalPrice)}`);
                        }
                        if (orderMonth === currentMonth && orderYear === currentYear) {
                            setMonthOrderPrice(`${parseInt(monthOrderPrice) + parseInt(order.totalPrice)}`);
                        }
                    }
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast]);

    return (
        <Box>
            <Box textAlign={'left'}>
                <Text fontSize={18} fontWeight={'600'}>
                    Tổng quan
                </Text>
                <HStack mt={8}>
                    {miniStatistics.map((miniStatistic, index) => (
                        <MiniStatisTics
                            key={index}
                            title={miniStatistic.title}
                            amount={miniStatistic.amount}
                            percentage={miniStatistic.percentage}
                            backgroundColor={miniStatistic.backgroundColor}
                            type={miniStatistic.type}
                        />
                    ))}
                </HStack>
                <Grid templateColumns="repeat(4, 1fr)" gap={4} mt={4}>
                    {orderStatistics.map((orderStatistic, index) => {
                        return (
                            <MiniStatisTics
                                key={index}
                                title={orderStatistic.title}
                                amount={orderStatistic.amount}
                                color={orderStatistic.color}
                                type={orderStatistic.type}
                            />
                        );
                    })}
                </Grid>
            </Box>

            <Box textAlign={'left'} mt={8}>
                <Text fontSize={18} fontWeight={'600'}>
                    Đơn hàng gần đây
                </Text>
                <Box mt={4}>
                    <TableContainer bgColor={'white'} borderRadius={5}>
                        <Table variant="simple">
                            <Thead>
                                <Tr>
                                    <Th>Thời gian</Th>
                                    <Th>Khách hàng</Th>
                                    <Th>Số điện thoại</Th>
                                    <Th>Email</Th>
                                    <Th>Tổng giá trị (VNĐ)</Th>
                                    <Th>Trạng thái</Th>
                                </Tr>
                            </Thead>
                            <Tbody>
                                {orders.length <= 0 && (
                                    <Tr>
                                        <Td colSpan={6} textAlign={'center'}>
                                            Chưa có đơn hàng nào
                                        </Td>
                                    </Tr>
                                )}
                                {orders.length > 0 &&
                                    orders.map((order, index) => {
                                        return (
                                            <Tr key={index}>
                                                <Td>{convertToDayMonthYear(order.date)}</Td>
                                                <Td>{order.User.name}</Td>
                                                <Td>{order.User.phone}</Td>
                                                <Td>{order.User.email}</Td>
                                                <Td>{separatePrice(order.totalPrice)}</Td>
                                                <Td>
                                                    <OrderStatus status={order.status} />
                                                </Td>
                                            </Tr>
                                        );
                                    })}
                            </Tbody>
                        </Table>
                    </TableContainer>
                </Box>
            </Box>
        </Box>
    );
}

export default Dashboard;
