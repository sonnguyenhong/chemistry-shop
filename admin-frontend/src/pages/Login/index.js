import { useState } from 'react';
import {
    Flex,
    Heading,
    Input,
    Button,
    InputGroup,
    Stack,
    InputLeftElement,
    chakra,
    Box,
    Link,
    Avatar,
    FormControl,
    FormHelperText,
    InputRightElement,
    useToast,
} from '@chakra-ui/react';
import { useDispatch } from 'react-redux';
import { login } from '../../redux/action_creators/AuthActions';
import { FaUserAlt, FaLock } from 'react-icons/fa';
import UserRole from '../../constants/UserRole';
import store from '../../store/store';
import { useNavigate } from 'react-router-dom';

const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);

const Login = () => {
    const [showPassword, setShowPassword] = useState(false);
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [isLoadingLogin, setIsLoadingLogin] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const toast = useToast();

    const handleShowClick = () => setShowPassword(!showPassword);

    const handleLogin = (e) => {
        e.preventDefault();
        setIsLoadingLogin(true);
        dispatch(login(username, password, UserRole.ADMIN)).then((response) => {
            if (store.getState().authReducer.isLoggedIn) {
                setIsLoadingLogin(false);
                navigate('/');
            } else {
                toast({
                    title: store.getState().authReducer.error.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
                setIsLoadingLogin(false);
            }
        });
    };

    return (
        <Flex
            flexDirection="column"
            width="100wh"
            height="100vh"
            backgroundColor="gray.200"
            justifyContent="center"
            alignItems="center"
            mt={-10}
            mb={-10}
        >
            <Stack flexDir="column" mb="2" justifyContent="center" alignItems="center">
                <Avatar bg="teal.500" />
                <Heading color="teal.400">Welcome</Heading>
                <Box minW={{ base: '90%', md: '468px' }}>
                    <form>
                        <Stack spacing={4} p="2rem" backgroundColor="whiteAlpha.900" boxShadow="md" borderRadius={10}>
                            <FormControl>
                                <InputGroup>
                                    <InputLeftElement pointerEvents="none" children={<CFaUserAlt color="gray.300" />} />
                                    <Input
                                        type="email"
                                        placeholder="Tài khoản"
                                        onChange={(e) => setUsername(e.target.value)}
                                    />
                                </InputGroup>
                            </FormControl>
                            <FormControl>
                                <InputGroup>
                                    <InputLeftElement
                                        pointerEvents="none"
                                        color="gray.300"
                                        children={<CFaLock color="gray.300" />}
                                    />
                                    <Input
                                        type={showPassword ? 'text' : 'password'}
                                        placeholder="Mật khẩu"
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                    <InputRightElement width="4.5rem">
                                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                                            {showPassword ? 'Ẩn' : 'Hiện'}
                                        </Button>
                                    </InputRightElement>
                                </InputGroup>
                                <FormHelperText textAlign="right">
                                    <Link>Forgot password?</Link>
                                </FormHelperText>
                            </FormControl>
                            <Button
                                borderRadius={5}
                                type="submit"
                                variant="solid"
                                colorScheme="teal"
                                width="full"
                                isLoading={isLoadingLogin}
                                onClick={handleLogin}
                            >
                                Login
                            </Button>
                        </Stack>
                    </form>
                </Box>
            </Stack>
        </Flex>
    );
};

export default Login;
