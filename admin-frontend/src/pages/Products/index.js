import {
    Box,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    Button,
    Image,
    Input,
    Icon,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    FormControl,
    FormLabel,
    useToast,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
} from '@chakra-ui/react';
import { useEffect, useRef, useState } from 'react';
import { FiPlus, FiUploadCloud } from 'react-icons/fi';
import ProductService from '../../services/ProductService';
import ApiStatus from '../../constants/ApiStatus';
import { separatePrice } from '../../utils/MoneyUtil';
import { useNavigate } from 'react-router-dom';

function Products() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [createDrawerOpen, setCreateDrawerOpen] = useState(false);

    const btnRef = useRef();

    const [products, setProducts] = useState([]);
    const [selectedProductId, setSeletedProductId] = useState();
    const [isLoadingDelete, setIsLoadingDelete] = useState(false);

    const [productId, setProductId] = useState();
    const [productCode, setProductCode] = useState();
    const [englishName, setEnglishName] = useState();
    const [vietnameseName, setVietnameseName] = useState();
    const [characteristics, setCharacteristics] = useState();
    const [brand, setBrand] = useState();
    const [origin, setOrigin] = useState();
    const [serial, setSerial] = useState();
    const [quantity, setQuantity] = useState();
    const [expireDate, setExpireDate] = useState();
    const [importDate, setImportDate] = useState();
    const [warehouse, setWarehouse] = useState();
    const [storageCondition, setStorageCondition] = useState();
    const [price, setPrice] = useState();
    const [image, setImage] = useState();

    const toast = useToast();
    const navigate = useNavigate();

    useEffect(() => {
        ProductService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setProducts(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast]);

    const handleCreateProduct = () => {
        ProductService.create(
            productId,
            productCode,
            englishName,
            vietnameseName,
            characteristics,
            brand,
            origin,
            serial,
            quantity,
            expireDate,
            importDate,
            warehouse,
            storageCondition,
            price,
            image,
        )
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    setCreateDrawerOpen(false);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    };

    const handleDetailClick = (_productId) => {
        navigate(`/products/${_productId}`);
    };

    const handleDeleteClick = (_productId) => {
        setIsLoadingDelete(true);
        ProductService.remove(_productId)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    setIsLoadingDelete(false);
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setIsLoadingDelete(false);
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    setSeletedProductId(null);
                    window.location.reload();
                }
            })
            .catch((err) => {
                setIsLoadingDelete(false);
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
        onClose();
    };

    return (
        <Box>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Xóa sản phẩm</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>Bạn có chắc chắn muốn xóa sản phẩm?</ModalBody>

                    <ModalFooter>
                        <Button bgColor={'#ddd'} mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button
                            isLoading={isLoadingDelete}
                            color={'white'}
                            bgColor={'red'}
                            _hover={{
                                opacity: 0.7,
                            }}
                            onClick={(e) => {
                                e.preventDefault();
                                handleDeleteClick(selectedProductId);
                            }}
                        >
                            Xác nhận xóa
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            {/* Create drawer  */}
            <Drawer
                isOpen={createDrawerOpen}
                placement="right"
                onClose={() => setCreateDrawerOpen(false)}
                finalFocusRef={btnRef}
                size={'lg'}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Thêm sản phẩm mới</DrawerHeader>

                    <DrawerBody>
                        <FormControl>
                            <FormLabel
                                htmlFor="file"
                                cursor={'pointer'}
                                padding={5}
                                borderWidth={2}
                                borderRadius={5}
                                borderStyle={'dotted'}
                                display={'flex'}
                                flexDir={'column'}
                                justifyContent={'center'}
                                alignItems={'center'}
                            >
                                <Icon as={FiUploadCloud} boxSize={30} mr={2} />
                                <Text> Tải lên hình ảnh sản phẩm</Text>
                            </FormLabel>
                            <Input
                                id="file"
                                type="file"
                                accept="image/*"
                                style={{ display: 'none' }}
                                onChange={(e) => setImage(e.target.files[0])}
                            />
                            <Box display={'flex'} justifyContent={'center'}>
                                {image && <Image src={URL.createObjectURL(image)} alt={image.name} width={'50%'} />}
                            </Box>
                        </FormControl>
                        <FormControl>
                            <FormLabel>Mã hàng</FormLabel>
                            <Input type="text" onChange={(e) => setProductId(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Mã sản phẩm</FormLabel>
                            <Input type="text" onChange={(e) => setProductCode(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Tên Tiếng Anh</FormLabel>
                            <Input type="text" onChange={(e) => setEnglishName(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Tên Tiếng Việt</FormLabel>
                            <Input type="text" onChange={(e) => setVietnameseName(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Đặc tính</FormLabel>
                            <Input type="text" onChange={(e) => setCharacteristics(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Hãng sản xuất</FormLabel>
                            <Input type="text" onChange={(e) => setBrand(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Xuất sứ</FormLabel>
                            <Input type="text" onChange={(e) => setOrigin(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Số Lot/Serial</FormLabel>
                            <Input type="text" onChange={(e) => setSerial(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Số lượng</FormLabel>
                            <Input type="number" onChange={(e) => setQuantity(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Ngày hết hạn</FormLabel>
                            <Input type="date" onChange={(e) => setExpireDate(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Ngày hàng về</FormLabel>
                            <Input type="date" onChange={(e) => setImportDate(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Kho hàng</FormLabel>
                            <Input type="text" onChange={(e) => setWarehouse(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Điều kiện bảo quản</FormLabel>
                            <Input type="text" onChange={(e) => setStorageCondition(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Đơn giá</FormLabel>
                            <Input type="text" onChange={(e) => setPrice(e.target.value)} />
                        </FormControl>
                    </DrawerBody>

                    <DrawerFooter>
                        <Button variant="outline" mr={3} onClick={() => setCreateDrawerOpen(false)} w={'50%'}>
                            Hủy bỏ
                        </Button>
                        <Button colorScheme="blue" w={'50%'} onClick={handleCreateProduct}>
                            Thêm sản phẩm
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>

            <Box mb={4}>
                <Text fontSize={25}>Danh sách sản phẩm</Text>
            </Box>
            <Box bgColor={'white'} borderRadius={5} padding={5} mb={4} display={'flex'} flexDir={'row'}>
                <Input placeholder="Tìm sản phẩm theo tên" size="md" mr={8} />
                <Button
                    display={'flex'}
                    alignItems={'center'}
                    paddingLeft={8}
                    paddingRight={8}
                    ref={btnRef}
                    onClick={(e) => setCreateDrawerOpen(true)}
                    backgroundColor={'#0e9f6e'}
                    color={'white'}
                    _hover={{
                        opacity: 0.7,
                    }}
                >
                    <Icon as={FiPlus} mr={2} boxSize={5} />
                    <Text>Tạo sản phẩm</Text>
                </Button>
            </Box>
            <TableContainer bgColor={'white'} borderRadius={5}>
                <Table variant="simple">
                    <Thead>
                        <Tr>
                            <Th>Mã sản phẩm</Th>
                            <Th>Tên sản phẩm</Th>
                            <Th>Hãng sản xuất</Th>
                            <Th>Đơn giá (VNĐ)</Th>
                            <Th>Thao tác</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {products.length <= 0 && (
                            <Tr>
                                <Td colSpan={5} textAlign={'center'}>
                                    Chưa có sản phẩm nào
                                </Td>
                            </Tr>
                        )}
                        {products.length > 0 &&
                            products.map((product, index) => {
                                return (
                                    <Tr key={index}>
                                        <Td>{product.productCode}</Td>
                                        <Td display={'flex'} flexDir={'row'} alignItems={'center'}>
                                            <Image
                                                src={
                                                    product.imageUrl
                                                        ? `${process.env.REACT_APP_BACKEND_URL}/${product.imageUrl}`
                                                        : 'https://bit.ly/dan-abramov'
                                                }
                                                alt={product.vietnameseName}
                                                borderRadius={10}
                                                boxSize={20}
                                                mr={4}
                                            />
                                            <Text>{product.vietnameseName}</Text>
                                        </Td>
                                        <Td>{product.brand}</Td>
                                        <Td>{separatePrice(product.price)}</Td>
                                        <Td>
                                            <Button
                                                bg={'#54A1E4'}
                                                color="white"
                                                mr={4}
                                                _hover={{ opacity: 0.7 }}
                                                fontSize={12}
                                                onClick={(e) => {
                                                    e.preventDefault();
                                                    handleDetailClick(product.id);
                                                }}
                                            >
                                                Chi tiết
                                            </Button>
                                            <Button
                                                bg={'red'}
                                                color="white"
                                                _hover={{ opacity: 0.7 }}
                                                fontSize={12}
                                                onClick={() => {
                                                    setSeletedProductId(product.id);
                                                    onOpen();
                                                }}
                                            >
                                                Xóa
                                            </Button>
                                        </Td>
                                    </Tr>
                                );
                            })}
                    </Tbody>
                </Table>
            </TableContainer>
        </Box>
    );
}

export default Products;
