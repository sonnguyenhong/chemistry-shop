import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Input,
    Spinner,
    Text,
    useToast,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ApiStatus from '../../constants/ApiStatus';
import CustomerService from '../../services/CustomerService';

function EmployeeDetail() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [employee, setEmployee] = useState();
    const { id } = useParams();
    const toast = useToast();

    const [username, setUsername] = useState();
    const [name, setName] = useState();

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        CustomerService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setEmployee(response.data);
                    setUsername(response.data.username);
                    setName(response.data.name);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [id, toast]);

    const handleUpdate = (e) => {
        e.preventDefault();
    };

    if (!employee) {
        return (
            <Box w={'100%'} h={'100%'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <Spinner />
            </Box>
        );
    } else {
        return (
            <Box pl={100} pr={100} w={'100%'}>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Sửa thông tin nhân viên</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Text>Bạn có chắc chắn muốn sửa thông tin nhân viên?</Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button mr={3} onClick={onClose} bgColor={'#eee'}>
                                Hủy
                            </Button>
                            <Button
                                isLoading={isLoading}
                                onClick={handleUpdate}
                                bgColor={'#189eff'}
                                color={'white'}
                                _hover={{
                                    opacity: 0.7,
                                }}
                            >
                                Xác nhận
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>

                {/* Description and add to cart */}
                <Text fontSize={25} mb={4}>
                    Thông tin chi tiết nhân viên {name}
                </Text>
                <Box
                    bgColor={'white'}
                    borderRadius={5}
                    w={'100%'}
                    p={4}
                    display={'flex'}
                    flexDir={'column'}
                    justifyContent={'space-between'}
                >
                    <Box mb={4}>
                        <Box mt={4} textAlign={'left'}>
                            <FormControl>
                                <FormLabel>Tài khoản</FormLabel>
                                <Input
                                    type="text"
                                    value={username}
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tên nhân viên</FormLabel>
                                <Input
                                    type="text"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </FormControl>
                        </Box>
                    </Box>
                    <Box>
                        <Button
                            isLoading={isLoading}
                            bgColor={'#189eff'}
                            color={'white'}
                            _hover={{ opacity: 0.9 }}
                            onClick={onOpen}
                        >
                            Sửa thông tin
                        </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}

export default EmployeeDetail;
