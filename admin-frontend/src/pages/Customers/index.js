import {
    Box,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    Button,
    Input,
    Icon,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    FormControl,
    FormLabel,
    DrawerFooter,
    useToast,
    Select,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
} from '@chakra-ui/react';
import { useEffect, useRef, useState } from 'react';
import { FiPlus } from 'react-icons/fi';
import { useNavigate } from 'react-router-dom';
import ApiStatus from '../../constants/ApiStatus';
import CustomerService from '../../services/CustomerService';
import { CUSTOMER_LEVEL, CUSTOMER_STATUS } from '../../constants/OtherConstant';

function Customers() {
    const btnRef = useRef();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [createDrawerOpen, setCreateDrawerOpen] = useState(false);
    const navigate = useNavigate();
    const toast = useToast();

    const [customers, setCustomers] = useState([]);
    const [selectedCustomerId, setSelectedCustomerId] = useState();
    const [isCreateLoading, setIsCreateLoading] = useState(false);
    const [isLoadingDelete, setIsLoadingDelete] = useState(false);

    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [phone, setPhone] = useState();
    const [name, setName] = useState();
    const [code, setCode] = useState();
    const [property, setProperty] = useState();
    const [level, setLevel] = useState();
    const [address, setAddress] = useState();
    const [status, setStatus] = useState();


    const handleClickDetail = (customerId) => {
        navigate(`/customers/${customerId}`);
    }

    const handleDeleteClick = (customerId) => {
        setIsLoadingDelete(true);
        CustomerService.remove(customerId)
            .then(response => {
                if (response.status !== ApiStatus.SUCCESS) {
                    setIsLoadingDelete(false);
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setIsLoadingDelete(false);
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    setSelectedCustomerId(null);
                    window.location.reload();
                }
            })
            .catch(err => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
                setIsLoadingDelete(false);
            })
    }

    const handleCreateCustomer = (e) => {
        e.preventDefault();
        setIsCreateLoading(true);
        const userData = {
            username, 
            email, 
            password, 
            phone, 
            name,
            code,
            property, 
            level,
            address,
            status,
        };

        CustomerService.create(userData)
            .then(response => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    setIsCreateLoading(false);
                } else {
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    setCreateDrawerOpen(false);
                    setIsCreateLoading(false);
                }
            })
            .catch(err => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
                setIsCreateLoading(false);
            })
    }

    useEffect(() => {
        CustomerService.getAll()
            .then(response => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setCustomers(response.data);
                }
            })
            .catch(err => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            })
    }, [])

    return (
        <Box>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Xóa khách hàng</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>Bạn có chắc chắn muốn xóa khách hàng?</ModalBody>

                    <ModalFooter>
                        <Button bgColor={'#ddd'} mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button
                            isLoading={isLoadingDelete}
                            color={'white'}
                            bgColor={'red'}
                            _hover={{
                                opacity: 0.7,
                            }}
                            onClick={(e) => {
                                e.preventDefault();
                                handleDeleteClick(selectedCustomerId);
                            }}
                        >
                            Xác nhận xóa
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            {/* Create customer drawer  */}
            <Drawer
                isOpen={createDrawerOpen}
                placement="right"
                onClose={() => setCreateDrawerOpen(false)}
                finalFocusRef={btnRef}
                size={'lg'}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Tạo khách hàng</DrawerHeader>

                    <DrawerBody>
                        <FormControl>
                            <FormLabel>Tài khoản</FormLabel>
                            <Input type="text" onChange={(e) => setUsername(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Email</FormLabel>
                            <Input type="email" onChange={(e) => setEmail(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Mật khẩu</FormLabel>
                            <Input type="password" onChange={(e) => setPassword(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Số điện thoại</FormLabel>
                            <Input type="text" onChange={(e) => setPhone(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Tên khách hàng</FormLabel>
                            <Input type="text" onChange={(e) => setName(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Mã khách hàng</FormLabel>
                            <Input type="text" onChange={(e) => setCode(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Thuộc tính</FormLabel>
                            <Input type="text" onChange={(e) => setProperty(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Tuyến</FormLabel>
                            <Select placeholder='Chọn tuyến' onChange={(e) => setLevel(e.target.value)} >
                                <option value={CUSTOMER_LEVEL.PROVINCE}>{CUSTOMER_LEVEL.PROVINCE}</option>
                                <option value={CUSTOMER_LEVEL.PRIVATE}>{CUSTOMER_LEVEL.PRIVATE}</option>
                                <option value={CUSTOMER_LEVEL.DISTRICT}>{CUSTOMER_LEVEL.DISTRICT}</option>
                                <option value={CUSTOMER_LEVEL.TW}>{CUSTOMER_LEVEL.TW}</option>
                            </Select>
                        </FormControl>
                        <FormControl>
                            <FormLabel>Địa chỉ</FormLabel>
                            <Input type="text" onChange={(e) => setAddress(e.target.value)} />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Trạng thái</FormLabel>
                            <Select placeholder='Trạng thái' onChange={(e) => setStatus(e.target.value)} >
                                <option value={CUSTOMER_STATUS.PENDING}>{CUSTOMER_STATUS.PENDING}</option>
                                <option value={CUSTOMER_STATUS.ACTIVE}>{CUSTOMER_STATUS.ACTIVE}</option>
                                <option value={CUSTOMER_STATUS.INACTIVE}>{CUSTOMER_STATUS.INACTIVE}</option>
                            </Select>
                        </FormControl>
                    </DrawerBody>

                    <DrawerFooter display={'flex'} flexDir={'column'}>
                        <Box display={'flex'} w={'100%'}>
                            <Button variant="outline" mr={3} onClick={() => setCreateDrawerOpen(false)} w={'50%'}>
                                Hủy bỏ
                            </Button>
                            <Button colorScheme="blue" w={'50%'} isLoading={isCreateLoading} onClick={handleCreateCustomer}>
                                Tạo khách hàng
                            </Button>
                        </Box>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
            <Box mb={4}>
                <Text fontSize={25}>Danh sách khách hàng</Text>
            </Box>
            <Box bgColor={'white'} borderRadius={5} padding={5} mb={4} display={'flex'} flexDir={'row'}>
                <Input placeholder="Tìm khách hàng theo tên" size="md" mr={8} />
                <Button
                    display={'flex'}
                    alignItems={'center'}
                    paddingLeft={8}
                    paddingRight={8}
                    ref={btnRef}
                    onClick={(e) => setCreateDrawerOpen(true)}
                    backgroundColor={'#0e9f6e'}
                    color={'white'}
                    _hover={{
                        opacity: 0.7,
                    }}
                >
                    <Icon as={FiPlus} mr={2} boxSize={5} />
                    <Text>Tạo khách hàng</Text>
                </Button>
            </Box>
            <TableContainer bgColor={'white'} borderRadius={5}>
                <Table variant="simple">
                    <Thead>
                        <Tr>
                            <Th>Mã khách hàng</Th>
                            <Th>Tên khách hàng</Th>
                            <Th>Địa chỉ</Th>
                            <Th>Email</Th>
                            <Th>Số điện thoại</Th>
                            <Th>Thao tác</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {
                            customers.length <= 0 && (
                                <Tr>
                                    <Td colSpan={6} textAlign={'center'}>
                                        Không có khách hàng nào
                                    </Td>
                                </Tr>
                            )
                        }
                        {
                            customers.length > 0 && 
                                customers.map((customer, index) => {
                                    console.log(customer);
                                    return (
                                        <Tr key={index}>
                                            <Td>{customer.code}</Td>
                                            <Td>{customer.name}</Td>
                                            <Td>{customer.address}</Td>
                                            <Td>{customer.email}</Td>
                                            <Td>{customer.phone}</Td>
                                            <Td>
                                                <Button bg={'#54A1E4'} color="white" mr={4} _hover={{ opacity: 0.7 }} fontSize={12} onClick={(e) => {
                                                    e.preventDefault();
                                                    handleClickDetail(customer.id);
                                                }}>
                                                    Chi tiết
                                                </Button>
                                                <Button 
                                                    bg={'red'} 
                                                    color="white" 
                                                    _hover={{ opacity: 0.7 }} 
                                                    fontSize={12}
                                                    onClick={() => {
                                                        setSelectedCustomerId(customer.id);
                                                        onOpen();
                                                    }}
                                                >
                                                    Xóa
                                                </Button>
                                            </Td>
                                        </Tr>
                                    )
                                })
                        }
                    </Tbody>
                </Table>
            </TableContainer>
        </Box>
    );
}

export default Customers;
