import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Input,
    Spinner,
    Text,
    useToast,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
    Select,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ApiStatus from '../../constants/ApiStatus';
import CustomerService from '../../services/CustomerService';
import { CUSTOMER_LEVEL, CUSTOMER_STATUS } from '../../constants/OtherConstant';

function CustomerDetail() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [customer, setCustomer] = useState();
    const { id } = useParams();
    const toast = useToast();
    const navigate = useNavigate();

    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [name, setName] = useState();
    const [code, setCode] = useState();
    const [property, setProperty] = useState();
    const [level, setLevel] = useState();
    const [address, setAddress] = useState();
    const [status, setStatus] = useState();

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        CustomerService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setCustomer(response.data);
                    setUsername(response.data.username);
                    setEmail(response.data.email);
                    setPhone(response.data.phone);
                    setName(response.data.name);
                    setCode(response.data.code);
                    setProperty(response.data.property);
                    setLevel(response.data.level);
                    setAddress(response.data.address);
                    setStatus(response.data.status);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [id, toast]);

    const handleUpdate = (e) => {
        e.preventDefault();
        const userData = {
            id,
            username, 
            email, 
            phone, 
            name,
            code,
            property, 
            level,
            address,
            status,
        };

        CustomerService.update(userData)
            .then(response => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    navigate('/customers');
                }
            })
            .catch(err => {
                setIsLoading(false);
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            })
    };

    if (!customer) {
        return (
            <Box w={'100%'} h={'100%'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <Spinner />
            </Box>
        );
    } else {
        return (
            <Box pl={100} pr={100} w={'100%'}>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Sửa thông tin khách hàng</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Text>Bạn có chắc chắn muốn sửa thông tin khách hàng?</Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button mr={3} onClick={onClose} bgColor={'#eee'}>
                                Hủy
                            </Button>
                            <Button
                                isLoading={isLoading}
                                onClick={handleUpdate}
                                bgColor={'#189eff'}
                                color={'white'}
                                _hover={{
                                    opacity: 0.7,
                                }}
                            >
                                Xác nhận
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>

                {/* Description and add to cart */}
                <Text fontSize={25} mb={4}>
                    Thông tin chi tiết khách hàng {code}
                </Text>
                <Box
                    bgColor={'white'}
                    borderRadius={5}
                    w={'100%'}
                    p={4}
                    display={'flex'}
                    flexDir={'column'}
                    justifyContent={'space-between'}
                >
                    <Box mb={4}>
                        <Text fontSize={18} fontWeight={'500'}>
                            {code}
                        </Text>
                        <Box mt={4} textAlign={'left'}>
                            <FormControl>
                                <FormLabel>Mã khách hàng</FormLabel>
                                <Input type="text" value={code} onChange={(e) => setCode(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tài khoản</FormLabel>
                                <Input
                                    type="text"
                                    value={username}
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Email</FormLabel>
                                <Input
                                    type="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Số điện thoại</FormLabel>
                                <Input
                                    type="text"
                                    value={phone}
                                    onChange={(e) => setPhone(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tên khách hàng</FormLabel>
                                <Input
                                    type="text"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Thuộc tính</FormLabel>
                                <Input
                                    type="text"
                                    value={property}
                                    onChange={(e) => setProperty(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tuyến</FormLabel>
                                <Select placeholder='Chọn tuyến' value={level} onChange={(e) => setLevel(e.target.value)} >
                                    <option value={CUSTOMER_LEVEL.PROVINCE}>{CUSTOMER_LEVEL.PROVINCE}</option>
                                    <option value={CUSTOMER_LEVEL.PRIVATE}>{CUSTOMER_LEVEL.PRIVATE}</option>
                                    <option value={CUSTOMER_LEVEL.DISTRICT}>{CUSTOMER_LEVEL.DISTRICT}</option>
                                    <option value={CUSTOMER_LEVEL.TW}>{CUSTOMER_LEVEL.TW}</option>
                                </Select>
                            </FormControl>  
                            <FormControl>
                                <FormLabel>Địa chỉ</FormLabel>
                                <Input
                                    type="text"
                                    value={address}
                                    onChange={(e) => setAddress(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Trạng thái</FormLabel>
                                <Select placeholder='Trạng thái' value={status} onChange={(e) => setStatus(e.target.value)} >
                                    <option value={CUSTOMER_STATUS.PENDING}>{CUSTOMER_STATUS.PENDING}</option>
                                    <option value={CUSTOMER_STATUS.ACTIVE}>{CUSTOMER_STATUS.ACTIVE}</option>
                                    <option value={CUSTOMER_STATUS.INACTIVE}>{CUSTOMER_STATUS.INACTIVE}</option>
                                </Select>
                            </FormControl>
                        </Box>
                    </Box>
                    <Box>
                        <Button
                            isLoading={isLoading}
                            bgColor={'#189eff'}
                            color={'white'}
                            _hover={{ opacity: 0.9 }}
                            onClick={onOpen}
                        >
                            Sửa thông tin
                        </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}

export default CustomerDetail;
