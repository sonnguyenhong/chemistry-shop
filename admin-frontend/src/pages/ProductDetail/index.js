import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Image,
    Input,
    Spinner,
    Text,
    useToast,
    Icon,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ProductService from '../../services/ProductService';
import ApiStatus from '../../constants/ApiStatus';
import { FiUploadCloud } from 'react-icons/fi';
import { separatePrice } from '../../utils/MoneyUtil';

function ProductDetail() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [product, setProduct] = useState();
    const { id } = useParams();
    const toast = useToast();
    const navigate = useNavigate();

    const [productId, setProductId] = useState();
    const [productCode, setProductCode] = useState();
    const [englishName, setEnglishName] = useState();
    const [vietnameseName, setVietnameseName] = useState();
    const [characteristics, setCharacteristics] = useState();
    const [brand, setBrand] = useState();
    const [origin, setOrigin] = useState();
    const [serial, setSerial] = useState();
    const [quantity, setQuantity] = useState();
    const [expireDate, setExpireDate] = useState();
    const [importDate, setImportDate] = useState();
    const [warehouse, setWarehouse] = useState();
    const [storageCondition, setStorageCondition] = useState();
    const [price, setPrice] = useState();
    const [image, setImage] = useState();

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        ProductService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setProduct(response.data);
                    console.log(response.data.imageUrl);
                    setProductId(response.data.productId);
                    setProductCode(response.data.productCode);
                    setEnglishName(response.data.englishName);
                    setVietnameseName(response.data.vietnameseName);
                    setCharacteristics(response.data.characteristics);
                    setBrand(response.data.brand);
                    setOrigin(response.data.origin);
                    setSerial(response.data.serial);
                    setQuantity(response.data.quantity);
                    setExpireDate(response.data.expireDate);
                    setImportDate(response.data.importDate);
                    setWarehouse(response.data.warehouse);
                    setStorageCondition(response.data.storageCondition);
                    setPrice(response.data.price);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [id, toast]);

    const handleUpdate = (e) => {
        e.preventDefault();
        setIsLoading(true);
        ProductService.update(
            id,
            productId,
            productCode,
            englishName,
            vietnameseName,
            characteristics,
            brand,
            origin,
            serial,
            quantity,
            expireDate,
            importDate,
            warehouse,
            storageCondition,
            price,
            image,
        )
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    navigate('/products');
                }
            })
            .catch((err) => {
                setIsLoading(false);
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
        onClose();
    };

    if (!product) {
        return (
            <Box w={'100%'} h={'100%'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <Spinner />
            </Box>
        );
    } else {
        return (
            <Box pl={100} pr={100} w={'100%'}>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Sửa thông tin sản phẩm</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Text>Bạn có chắc chắn muốn sửa thông tin sản phẩm?</Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button mr={3} onClick={onClose} bgColor={'#eee'}>
                                Hủy
                            </Button>
                            <Button
                                isLoading={isLoading}
                                onClick={handleUpdate}
                                bgColor={'#189eff'}
                                color={'white'}
                                _hover={{
                                    opacity: 0.7,
                                }}
                            >
                                Xác nhận
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>

                {/* Description and add to cart */}
                <Text fontSize={25} mb={4}>
                    Thông tin chi tiết sản phẩm {product.vietnameseName}
                </Text>
                <Box
                    bgColor={'white'}
                    borderRadius={5}
                    w={'100%'}
                    p={4}
                    display={'flex'}
                    flexDir={'column'}
                    justifyContent={'space-between'}
                >
                    <Box mb={4}>
                        <Text fontSize={18} fontWeight={'500'}>
                            {product.vietnameseName}
                        </Text>
                        <Box mt={4} textAlign={'left'}>
                            <FormControl>
                                <FormLabel
                                    htmlFor="file"
                                    cursor={'pointer'}
                                    padding={5}
                                    borderWidth={2}
                                    borderRadius={5}
                                    borderStyle={'dotted'}
                                    display={'flex'}
                                    flexDir={'column'}
                                    justifyContent={'center'}
                                    alignItems={'center'}
                                >
                                    <Icon as={FiUploadCloud} boxSize={30} mr={2} />
                                    <Text> Tải lên hình ảnh sản phẩm</Text>
                                </FormLabel>
                                <Input
                                    id="file"
                                    type="file"
                                    accept="image/*"
                                    style={{ display: 'none' }}
                                    onChange={(e) => setImage(e.target.files[0])}
                                />
                                <Box display={'flex'} justifyContent={'center'}>
                                    {image && <Image src={URL.createObjectURL(image)} alt={image.name} width={'50%'} />}
                                    {!image && product.imageUrl && (
                                        <Image src={`${process.env.REACT_APP_BACKEND_URL}/${product.imageUrl}`} />
                                    )}
                                </Box>
                            </FormControl>
                            <FormControl>
                                <FormLabel>Mã hàng</FormLabel>
                                <Input type="text" value={productId} onChange={(e) => setProductId(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Mã sản phẩm</FormLabel>
                                <Input
                                    type="text"
                                    value={productCode}
                                    onChange={(e) => setProductCode(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tên Tiếng Anh</FormLabel>
                                <Input
                                    type="text"
                                    value={englishName}
                                    onChange={(e) => setEnglishName(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Tên Tiếng Việt</FormLabel>
                                <Input
                                    type="text"
                                    value={vietnameseName}
                                    onChange={(e) => setVietnameseName(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Đặc tính</FormLabel>
                                <Input
                                    type="text"
                                    value={characteristics}
                                    onChange={(e) => setCharacteristics(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Hãng sản xuất</FormLabel>
                                <Input type="text" value={brand} onChange={(e) => setBrand(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Xuất sứ</FormLabel>
                                <Input type="text" value={origin} onChange={(e) => setOrigin(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Số Lot/Serial</FormLabel>
                                <Input type="text" value={serial} onChange={(e) => setSerial(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Số lượng</FormLabel>
                                <Input type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Ngày hết hạn</FormLabel>
                                <Input
                                    type="date"
                                    value={expireDate.substring(0, 10)}
                                    onChange={(e) => setExpireDate(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Ngày hàng về</FormLabel>
                                <Input
                                    type="date"
                                    value={importDate.substring(0, 10)}
                                    onChange={(e) => setImportDate(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Kho hàng</FormLabel>
                                <Input type="text" value={warehouse} onChange={(e) => setWarehouse(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Điều kiện bảo quản</FormLabel>
                                <Input
                                    type="text"
                                    value={storageCondition}
                                    onChange={(e) => setStorageCondition(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Đơn giá</FormLabel>
                                <Input type="text" value={price} onChange={(e) => setPrice(e.target.value)} />
                            </FormControl>
                        </Box>
                    </Box>
                    <Box>
                        <Button
                            isLoading={isLoading}
                            bgColor={'#189eff'}
                            color={'white'}
                            _hover={{ opacity: 0.9 }}
                            onClick={onOpen}
                        >
                            Sửa thông tin
                        </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}

export default ProductDetail;
