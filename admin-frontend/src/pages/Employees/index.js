import {
    Box,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    FormControl,
    FormLabel,
    Input,
    DrawerFooter,
    Icon,
    useToast,
} from '@chakra-ui/react';
import { useEffect, useRef, useState } from 'react';
import { FiPlus } from 'react-icons/fi';
import ApiStatus from '../../constants/ApiStatus';
import EmployeeService from '../../services/EmployeeService';
import { useNavigate } from 'react-router-dom';

function Employees() {
    const btnRef = useRef();
    const [createDrawerOpen, setCreateDrawerOpen] = useState(false);
    const [employees, setEmployees] = useState([]);
    const toast = useToast();
    const navigate = useNavigate();

    const handleDetailClick = (id) => {
        navigate(`/employees/${id}`);
    }
    
    useEffect(() => {
        EmployeeService.getAll()
            .then(response => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setEmployees(response.data);
                }
            })
            .catch(err => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            })
    }, []);

    return (
        <Box>
            {/* Create employee drawer  */}
            <Drawer
                isOpen={createDrawerOpen}
                placement="right"
                onClose={() => setCreateDrawerOpen(false)}
                finalFocusRef={btnRef}
                size={'lg'}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Tạo nhân viên</DrawerHeader>

                    <DrawerBody>
                        <FormControl>
                            <FormLabel>Tài khoản</FormLabel>
                            <Input type="text" />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Mật khẩu</FormLabel>
                            <Input type="password" />
                        </FormControl>
                        <FormControl>
                            <FormLabel>Tên nhân viên</FormLabel>
                            <Input type="text" />
                        </FormControl>
                    </DrawerBody>

                    <DrawerFooter display={'flex'} flexDir={'column'}>
                        <Box display={'flex'} w={'100%'}>
                            <Button variant="outline" mr={3} onClick={() => setCreateDrawerOpen(false)} w={'50%'}>
                                Hủy bỏ
                            </Button>
                            <Button colorScheme="blue" w={'50%'}>
                                Tạo nhân viên
                            </Button>
                        </Box>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
            <Box mb={4}>
                <Text fontSize={25}>Danh sách nhân viên</Text>
            </Box>
            <Box bgColor={'white'} borderRadius={5} padding={5} mb={4} display={'flex'} flexDir={'row'}>
                <Input placeholder="Tìm nhân viên theo tên" size="md" mr={8} />
                <Button
                    display={'flex'}
                    alignItems={'center'}
                    paddingLeft={8}
                    paddingRight={8}
                    ref={btnRef}
                    onClick={(e) => setCreateDrawerOpen(true)}
                    backgroundColor={'#0e9f6e'}
                    color={'white'}
                    _hover={{
                        opacity: 0.7,
                    }}
                >
                    <Icon as={FiPlus} mr={2} boxSize={5} />
                    <Text>Tạo nhân viên</Text>
                </Button>
            </Box>
            <TableContainer bgColor={'white'} borderRadius={5}>
                <Table variant="simple">
                    <Thead>
                        <Tr>
                            <Th>Tên nhân viên</Th>
                            <Th>Phân quyền</Th>
                            <Th>Tài khoản</Th>
                            <Th>Thao tác</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {
                            employees.length <= 0 && (
                                <Tr>
                                    <Td colSpan={4} textAlign={'center'}>Không có nhân viên nào</Td>
                                </Tr>
                            )
                        }
                        {
                            employees.length > 0 && 
                                employees.map((employee, index) => {
                                    return (
                                        <Tr key={index}>
                                            <Td>{employee.name}</Td>
                                            <Td>Admin</Td>
                                            <Td>{employee.username}</Td>
                                            <Td>
                                                <Button bg={'#54A1E4'} color="white" mr={4} _hover={{ opacity: 0.7 }} fontSize={12} onClick={(e) => {
                                                    e.preventDefault();
                                                    handleDetailClick(employee.id);
                                                }}>
                                                    Chi tiết
                                                </Button>
                                                <Button bg={'red'} color="white" _hover={{ opacity: 0.7 }} fontSize={12}>
                                                    Xóa
                                                </Button>
                                            </Td>
                                        </Tr>
                                    )
                                })
                        }
                    </Tbody>
                </Table>
            </TableContainer>
        </Box>
    );
}

export default Employees;
