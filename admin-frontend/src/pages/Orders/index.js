import {
    Box,
    Table,
    TableContainer,
    Tbody,
    Td,
    Text,
    Th,
    Thead,
    Tr,
    Button,
    useToast,
    Input,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    FormControl,
    FormLabel,
    Icon,
    DrawerFooter,
    Divider,
    Select,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
} from '@chakra-ui/react';
import OrderStatus from '../../components/OrderStatus';
import { useEffect, useRef, useState } from 'react';
import OrderService from '../../services/OrderService';
import { convertToDayMonthYear } from '../../utils/DateUtil';
import { separatePrice } from '../../utils/MoneyUtil';
import { FiPlus } from 'react-icons/fi';
import ProductService from '../../services/ProductService';
import ApiStatus from '../../constants/ApiStatus';
import { useNavigate } from 'react-router-dom';

function Orders() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [orders, setOrders] = useState([]);
    const [products, setProducts] = useState([]);
    const [createDrawerOpen, setCreateDrawerOpen] = useState(false);
    const [numOrderProducts, setNumOrderProducts] = useState(0);
    const [selectedOrderId, setSelectedOrderId] = useState();
    const toast = useToast();
    const navigate = useNavigate();
    const btnRef = useRef();

    const handleDetailClick = (orderId) => {
        navigate(`/orders/${orderId}`);
    };

    const handleDeleteClick = (orderId) => {};

    useEffect(() => {
        OrderService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setOrders(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });

        ProductService.getAll()
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setProducts(response.data);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [toast]);

    return (
        <Box>
            {/* Delete modal  */}
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Xóa đơn hàng</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>Bạn có chắc chắn muốn xóa đơn hàng?</ModalBody>

                    <ModalFooter>
                        <Button bgColor={'#ddd'} mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button
                            color={'white'}
                            bgColor={'red'}
                            _hover={{
                                opacity: 0.7,
                            }}
                            onClick={(e) => {
                                e.preventDefault();
                                handleDeleteClick(selectedOrderId);
                            }}
                        >
                            Xác nhận xóa
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            {/* Create order Drawer */}
            <Drawer
                isOpen={createDrawerOpen}
                placement="right"
                onClose={() => setCreateDrawerOpen(false)}
                finalFocusRef={btnRef}
                size={'lg'}
            >
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Tạo đơn đặt hàng</DrawerHeader>

                    <DrawerBody>
                        <FormControl>
                            <FormLabel>Khách hàng</FormLabel>
                            <Input type="text" />
                        </FormControl>
                        {Array.from({ length: numOrderProducts }).map((_, index) => (
                            <Box key={index}>
                                <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
                                    <FormControl w={'48%'}>
                                        <FormLabel>Mã sản phẩm</FormLabel>
                                        <Select placeholder="Chọn sản phẩm">
                                            {products.map((product, i) => (
                                                <option key={i} value={`${product.productCode}-${product.price}`}>
                                                    {product.productCode} - {product.vietnameseName}
                                                </option>
                                            ))}
                                        </Select>
                                    </FormControl>
                                    <FormControl w={'48%'}>
                                        <FormLabel>Số lượng</FormLabel>
                                        <Input type="number" />
                                    </FormControl>
                                </Box>
                                <Button mt={2} w={'100%'}>
                                    <Text>Xóa sản phẩm</Text>
                                </Button>
                                <Divider mt={2} />
                            </Box>
                        ))}
                        <Button
                            mt={4}
                            bgColor={'#54a1e4'}
                            color={'#fff'}
                            w={'100%'}
                            _hover={{ opacity: 0.7 }}
                            onClick={(e) => {
                                e.preventDefault();
                                setNumOrderProducts(numOrderProducts + 1);
                            }}
                        >
                            <Icon as={FiPlus} mr={2} boxSize={5} />
                            Thêm sản phẩm khác
                        </Button>
                    </DrawerBody>

                    <DrawerFooter display={'flex'} flexDir={'column'}>
                        <Box>
                            <Text fontSize={20} fontWeight={'500'}>
                                Thành tiền (VNĐ): {separatePrice(`1000000`)}
                            </Text>
                        </Box>
                        <Divider mb={2} mt={2} />
                        <Box display={'flex'} w={'100%'}>
                            <Button variant="outline" mr={3} onClick={() => setCreateDrawerOpen(false)} w={'50%'}>
                                Hủy bỏ
                            </Button>
                            <Button colorScheme="blue" w={'50%'}>
                                Tạo đơn hàng
                            </Button>
                        </Box>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
            <Box mb={4}>
                <Text fontSize={25}>Danh sách đơn hàng</Text>
            </Box>
            <Box bgColor={'white'} borderRadius={5} padding={5} mb={4} display={'flex'} flexDir={'row'}>
                <Input placeholder="Tìm đơn hàng theo tên khách hàng" size="md" mr={8} />
                <Button
                    display={'flex'}
                    alignItems={'center'}
                    paddingLeft={8}
                    paddingRight={8}
                    ref={btnRef}
                    onClick={(e) => setCreateDrawerOpen(true)}
                    backgroundColor={'#0e9f6e'}
                    color={'white'}
                    _hover={{
                        opacity: 0.7,
                    }}
                >
                    <Icon as={FiPlus} mr={2} boxSize={5} />
                    <Text>Tạo đơn hàng</Text>
                </Button>
            </Box>
            <TableContainer bgColor={'white'} borderRadius={5}>
                <Table variant="simple">
                    <Thead>
                        <Tr>
                            <Th>Thời gian</Th>
                            <Th>Khách hàng</Th>
                            <Th>Số điện thoại</Th>
                            <Th>Email</Th>
                            <Th>Tổng giá trị (VNĐ)</Th>
                            <Th>Trạng thái</Th>
                            <Th>Thao tác</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {orders.length <= 0 && (
                            <Tr>
                                <Td colSpan={7} textAlign={'center'}>
                                    Không có đơn hàng nào
                                </Td>
                            </Tr>
                        )}

                        {orders.map((order, index) => (
                            <Tr key={index}>
                                <Td>{convertToDayMonthYear(order.createdAt)}</Td>
                                <Td>{order.User.name}</Td>
                                <Td>{order.User.phone}</Td>
                                <Td>{order.User.email}</Td>
                                <Td>{separatePrice(order.totalPrice)}</Td>
                                <Td>
                                    <OrderStatus status={order.status} />
                                </Td>
                                <Td>
                                    <Button
                                        bg={'#54A1E4'}
                                        color="white"
                                        mr={4}
                                        _hover={{ opacity: 0.7 }}
                                        fontSize={12}
                                        onClick={(e) => {
                                            e.preventDefault();
                                            handleDetailClick(order.id);
                                        }}
                                    >
                                        Chi tiết
                                    </Button>
                                    <Button
                                        bg={'red'}
                                        color="white"
                                        _hover={{ opacity: 0.7 }}
                                        fontSize={12}
                                        onClick={() => {
                                            setSelectedOrderId(order.id);
                                            onOpen();
                                        }}
                                    >
                                        Xóa
                                    </Button>
                                </Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </TableContainer>
        </Box>
    );
}

export default Orders;
