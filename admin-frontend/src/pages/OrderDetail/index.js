import {
    Box,
    Button,
    FormControl,
    FormLabel,
    Image,
    Input,
    Spinner,
    Text,
    useToast,
    Icon,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
    TableContainer,
    Table,
    Thead,
    Tr,
    Th,
    Tbody,
    Td,
    Tfoot,
    Select,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import ProductService from '../../services/ProductService';
import ApiStatus from '../../constants/ApiStatus';
import { FiUploadCloud } from 'react-icons/fi';
import { separatePrice } from '../../utils/MoneyUtil';
import OrderService from '../../services/OrderService';
import OrderStatusConstant from '../../constants/OrderStatus';

function OrderDetail() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [order, setOrder] = useState();
    const { id } = useParams();
    const toast = useToast();
    const navigate = useNavigate();

    const [orderCode, setOrderCode] = useState();
    const [orderDate, setOrderDate] = useState();
    const [totalPrice, setTotalPrice] = useState();
    const [status, setStatus] = useState();
    const [orderProducts, setOrderProducts] = useState([]);

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        OrderService.getById(id)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    setOrder(response.data);
                    setOrderCode(response.data.code);
                    setOrderDate(response.data.date);
                    setStatus(response.data.status);
                    setOrderProducts(response.data.OrderDetails);
                    setTotalPrice(response.data.totalPrice);
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            });
    }, [id, toast]);

    const handleUpdate = (e) => {
        e.preventDefault();
        OrderService.update(id, status)
            .then((response) => {
                if (response.status !== ApiStatus.SUCCESS) {
                    toast({
                        title: response.message,
                        status: 'error',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                } else {
                    toast({
                        title: response.message,
                        status: 'success',
                        position: 'bottom',
                        duration: 5000,
                        isClosable: true,
                    });
                    navigate('/orders');
                }
            })
            .catch((err) => {
                toast({
                    title: err.message,
                    status: 'error',
                    position: 'bottom',
                    duration: 5000,
                    isClosable: true,
                });
            })
    };

    if (!order) {
        return (
            <Box w={'100%'} h={'100%'} display={'flex'} justifyContent={'center'} alignItems={'center'}>
                <Spinner />
            </Box>
        );
    } else {
        return (
            <Box pl={100} pr={100} w={'100%'}>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Sửa thông tin đơn hàng</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Text>Bạn có chắc chắn muốn sửa thông tin đơn hàng?</Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button mr={3} onClick={onClose} bgColor={'#eee'}>
                                Hủy
                            </Button>
                            <Button
                                isLoading={isLoading}
                                onClick={handleUpdate}
                                bgColor={'#189eff'}
                                color={'white'}
                                _hover={{
                                    opacity: 0.7,
                                }}
                            >
                                Xác nhận
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>

                {/* Description and add to cart */}
                <Text fontSize={25} mb={4}>
                    Thông tin chi tiết đơn hàng {order.code}
                </Text>
                <Box
                    bgColor={'white'}
                    borderRadius={5}
                    w={'100%'}
                    p={4}
                    display={'flex'}
                    flexDir={'column'}
                    justifyContent={'space-between'}
                >
                    <Box mb={4}>
                        <Text fontSize={18} fontWeight={'500'}>
                            {order.code}
                        </Text>
                        <Box mt={4} textAlign={'left'}>
                            <FormControl>
                                <FormLabel>Mã đơn hàng</FormLabel>
                                <Input type="text" value={orderCode} onChange={(e) => setOrderCode(e.target.value)} />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Ngày đặt hàng</FormLabel>
                                <Input
                                    type="date"
                                    value={orderDate.substring(0, 10)}
                                    onChange={(e) => setOrderDate(e.target.value)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Trạng thái đơn hàng</FormLabel>
                                <Select value={status} onChange={(e) => setStatus(e.target.value)}>
                                    <option value={OrderStatusConstant.PENDING}>{OrderStatusConstant.PENDING}</option>
                                    <option value={OrderStatusConstant.ACCEPT}>{OrderStatusConstant.ACCEPT}</option>
                                    <option value={OrderStatusConstant.PREPARING}>
                                        {OrderStatusConstant.PREPARING}
                                    </option>
                                    <option value={OrderStatusConstant.DELIVERING}>
                                        {OrderStatusConstant.DELIVERING}
                                    </option>
                                    <option value={OrderStatusConstant.DELIVERED}>
                                        {OrderStatusConstant.DELIVERED}
                                    </option>
                                    <option value={OrderStatusConstant.CLOSED}>{OrderStatusConstant.CLOSED}</option>
                                    <option value={OrderStatusConstant.REJECT}>{OrderStatusConstant.REJECT}</option>
                                </Select>
                            </FormControl>
                            <FormControl mt={4}>
                                <FormLabel>Danh sách mặt hàng</FormLabel>
                                <TableContainer>
                                    <Table variant="simple">
                                        <Thead>
                                            <Tr>
                                                <Th>Tên mặt hàng</Th>
                                                <Th>Đơn giá (VNĐ)</Th>
                                                <Th>Số lượng</Th>
                                                <Th>Thành tiền (VNĐ)</Th>
                                            </Tr>
                                        </Thead>
                                        <Tbody>
                                            {order.OrderDetails.length <= 0 && (
                                                <Tr mt={4}>
                                                    <Td colSpan={4} textAlign={'center'} fontWeight={'450'}>
                                                        Không có sản phẩm
                                                    </Td>
                                                </Tr>
                                            )}
                                            {order.OrderDetails.map((orderDetail, index) => {
                                                return (
                                                    <Tr key={index}>
                                                        <Td display={'flex'} alignItems={'center'}>
                                                            <Image
                                                                src="https://bit.ly/dan-abramov"
                                                                alt="Dan Abramov"
                                                                borderRadius={10}
                                                                boxSize={20}
                                                                mr={4}
                                                            />
                                                            <Text>{orderDetail.Product.vietnameseName}</Text>
                                                        </Td>
                                                        <Td>{separatePrice(orderDetail.Product.price)}</Td>
                                                        <Td>{orderDetail.quantity}</Td>
                                                        <Td>
                                                            {separatePrice(
                                                                (
                                                                    parseInt(orderDetail.Product.price) *
                                                                    orderDetail.quantity
                                                                ).toString(),
                                                            )}
                                                        </Td>
                                                    </Tr>
                                                );
                                            })}
                                        </Tbody>
                                        <Tfoot>
                                            <Tr>
                                                <Th fontSize={20} textAlign={'center'} colSpan={3}>
                                                    Tổng giá tiền
                                                </Th>
                                                <Th fontSize={20}>{separatePrice(order.totalPrice)}</Th>
                                            </Tr>
                                        </Tfoot>
                                    </Table>
                                </TableContainer>
                            </FormControl>
                        </Box>
                    </Box>
                    <Box>
                        <Button
                            isLoading={isLoading}
                            bgColor={'#189eff'}
                            color={'white'}
                            _hover={{ opacity: 0.9 }}
                            onClick={onOpen}
                        >
                            Sửa thông tin
                        </Button>
                    </Box>
                </Box>
            </Box>
        );
    }
}

export default OrderDetail;
