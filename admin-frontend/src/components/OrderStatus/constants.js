import OrderStatusConstant from '../../constants/OrderStatus';

export const OrderStatusStyle = {
    [OrderStatusConstant.PENDING]: {
        color: '#FFC107',
        backgroundColor: '#FFE082',
    },
    [OrderStatusConstant.ACCEPT]: {
        color: '#8BC34A',
        backgroundColor: '#C8E6C9',
    },
    [OrderStatusConstant.PREPARING]: {
        color: '#00BCD4',
        backgroundColor: '#B2EBF2',
    },
    [OrderStatusConstant.DELIVERING]: {
        color: '#795548',
        backgroundColor: '#FFC107',
    },
    [OrderStatusConstant.DELIVERED]: {
        color: '#00C853',
        backgroundColor: '#C8E6C9',
    },
    [OrderStatusConstant.CLOSED]: {
        color: '#616161 ',
        backgroundColor: '#BDBDBD ',
    },
    [OrderStatusConstant.REJECT]: {
        color: '#F44336',
        backgroundColor: '#FFCDD2',
    },
};
