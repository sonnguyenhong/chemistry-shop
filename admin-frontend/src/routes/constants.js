import MainLayout from '../layout/MainLayout';
import DefaultLayout from '../layout/DefaultLayout';
import Customers from '../pages/Customers';
import Dashboard from '../pages/Dashboard';
import Employees from '../pages/Employees';
import Orders from '../pages/Orders';
import Products from '../pages/Products';
import Login from '../pages/Login';
import ProductDetail from '../pages/ProductDetail';
import OrderDetail from '../pages/OrderDetail';
import CustomerDetail from '../pages/CustomerDetail';
import EmployeeDetail from '../pages/EmployeeDetail';

export const publicUrl = [
    {
        path: '/login',
        element: Login,
        layout: DefaultLayout,
    },
];

export const privateUrl = [
    {
        path: '/',
        element: Dashboard,
        layout: MainLayout,
    },
    {
        path: '/products',
        element: Products,
        layout: MainLayout,
    },
    {
        path: '/products/:id',
        element: ProductDetail,
        layout: MainLayout,
    },
    {
        path: '/orders',
        element: Orders,
        layout: MainLayout,
    },
    {
        path: '/orders/:id',
        element: OrderDetail,
        layout: MainLayout,
    },
    {
        path: '/customers',
        element: Customers,
        layout: MainLayout,
    },
    {
        path: '/customers/:id',
        element: CustomerDetail,
        layout: MainLayout,
    },
    {
        path: '/employees',
        element: Employees,
        layout: MainLayout,
    },
    {
        path: '/employees/:id',
        element: EmployeeDetail,
        layout: MainLayout,
    }
];
