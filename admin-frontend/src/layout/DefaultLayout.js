import { Box } from '@chakra-ui/react';

function DefaultLayout({ children }) {
    return (
        <Box>
            <Box mt={10} mb={10} minH={400} w={'100%'}>
                {children}
            </Box>
        </Box>
    );
}

export default DefaultLayout;
