export const separatePrice = (stringMoney) => {
    const MAX_COUNTER = 3;
    let counter = 0;
    let newStringMoney = '';
    const length = stringMoney.length;

    if (length <= 3) {
        return stringMoney;
    }

    for (let i = length - 1; i >= 0; i--) {
        counter++;
        newStringMoney = `${stringMoney[i]}${newStringMoney}`;
        if (counter === MAX_COUNTER) {
            counter = 0;
            if (i > 0) {
                newStringMoney = `.${newStringMoney}`;
            }
        }
    }
    return newStringMoney;
};
