export const NULL_PRODUCT = 'null_product';
export const NULL_QUANTITY = 'null_quantity';
export const NULL_UNITPRICE = 'null_unitprice';

export const CUSTOMER_LEVEL = {
    PROVINCE: 'Tuyến tỉnh',
    PRIVATE: 'PK tư nhân',
    DISTRICT: 'Tuyến huyện',
    TW: 'Tuyến Trung ương',
}

export const CUSTOMER_STATUS = {
    PENDING: 'PENDING',
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE',
}