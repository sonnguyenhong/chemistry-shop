import axios from '../configs/axios.config';

const EmployeeService = {
    getAll: async () => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get('/api/v1/employees', configs);
        return response.data;
    },
    getById: async (id) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get(`/api/v1/employees/${id}`, configs);
        return response.data;
    }
};

export default EmployeeService;
