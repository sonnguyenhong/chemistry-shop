import axios from '../configs/axios.config';

const ProductService = {
    getAll: async () => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get('/api/v1/products', configs);
        return response.data;
    },
    getById: async (id) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'application/json',
            },
        };

        const response = await axios.get(`/api/v1/products/${id}`, configs);
        return response.data;
    },
    create: async (
        productId,
        productCode,
        englishName,
        vietnameseName,
        characteristics,
        brand,
        origin,
        serial,
        quantity,
        expireDate,
        importDate,
        warehouse,
        storageCondition,
        price,
        image,
    ) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'multipart/form-data',
            },
        };

        const data = {
            productId,
            productCode,
            englishName,
            vietnameseName,
            characteristics,
            brand,
            origin,
            serial,
            quantity,
            expireDate,
            importDate,
            warehouse,
            storageCondition,
            price,
            image,
        };

        const response = await axios.post('/api/v1/products', data, configs);
        return response.data;
    },
    update: async (
        id,
        productId,
        productCode,
        englishName,
        vietnameseName,
        characteristics,
        brand,
        origin,
        serial,
        quantity,
        expireDate,
        importDate,
        warehouse,
        storageCondition,
        price,
        image,
    ) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'multipart/form-data',
            },
        };

        const data = {
            productId,
            productCode,
            englishName,
            vietnameseName,
            characteristics,
            brand,
            origin,
            serial,
            quantity,
            expireDate,
            importDate,
            warehouse,
            storageCondition,
            price,
            image,
        };

        const response = await axios.put(`/api/v1/products/${id}`, data, configs);
        return response.data;
    },
    remove: async (id) => {
        const token = JSON.parse(localStorage.getItem('user')).token;
        const bearerToken = `Bearer ${token}`;
        const configs = {
            headers: {
                Authorization: bearerToken,
                'Content-Type': 'multipart/form-data',
            },
        };

        const response = await axios.delete(`/api/v1/products/${id}`, configs);
        return response.data;
    },
};

export default ProductService;
